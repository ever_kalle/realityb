﻿Shader "Portal/PortalSurfaceShaderSimple" {
	Properties {
		_MainTex ("Albedo (RGB)", 2D) = "white" {}
	}
	SubShader {
		Tags {"Queue"="AlphaTest" "RenderType"="TransparentCutout" "IgnoreProjector"="True" }
		LOD 200

		Pass {
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			#include "UnityCG.cginc"
			struct v2f
			{
				float4 pos : SV_POSITION;
				float4 uv : TEXCOORD0;
			};

			v2f vert(float4 pos : POSITION)
			{
				v2f o;
				o.pos = mul (UNITY_MATRIX_MVP, pos);
				o.uv = ComputeScreenPos (o.pos);
				return o;
			}
			sampler2D _MainTex;
			fixed4 frag(v2f i) : SV_Target
			{
				return tex2Dproj(_MainTex, i.uv);
			}
			ENDCG
	    }
	}
	FallBack "Diffuse"
}
