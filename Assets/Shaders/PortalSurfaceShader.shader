﻿Shader "Portal/PortalSurfaceShader" {
	Properties {
		_Color ("Color", Color) = (1,1,1,1)
		_MainTex ("Albedo (RGB)", 2D) = "white" {}
		_ShapeTex ("Portal Shape (RGB)", 2D) = "white" {}
		_NoiseTex ("Noise Texture (RGB)", 2D) = "white" {}
		_InnerEdgeColor ("Inner Edge Color", Color) = (1,1,1,1)
		_EdgeColor ("Edge Color", Color) = (1,1,1,1)
		_Glossiness ("Smoothness", Range(0,1)) = 0.5
		_Metallic ("Metallic", Range(0,1)) = 0.0
		_DetailTex ("Detail Texture (RGB)", 2D) = "black" {}
	}
	SubShader {
		Tags {"Queue"="AlphaTest" "RenderType"="TransparentCutout" "IgnoreProjector"="True" }
		LOD 200

		Pass {
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			#include "UnityCG.cginc"
			struct v2f
			{
				float2 uv : TEXCOORD0;
				float4 refl : TEXCOORD1;
				float4 pos : SV_POSITION;
			};



			float4 _MainTex_ST;
			v2f vert(float4 pos : POSITION, float2 uv : TEXCOORD0)
			{
				v2f o;
				o.pos = mul (UNITY_MATRIX_MVP, pos);
				o.uv = TRANSFORM_TEX(uv, _MainTex);
				o.refl = ComputeScreenPos (o.pos);
				return o;
			}
			sampler2D _MainTex;
			sampler2D _ShapeTex;
			sampler2D _NoiseTex;
			sampler2D _DetailTex;
			fixed4 _EdgeColor;
			fixed4 _InnerEdgeColor;
			fixed4 frag(v2f i) : SV_Target
			{
				fixed4 tex = tex2D(_MainTex, i.uv);
				fixed4 refl = tex2Dproj(_MainTex, UNITY_PROJ_COORD(i.refl));
				fixed4 detail = tex2D(_DetailTex, i.uv);
				fixed4 noise = tex2D (_NoiseTex, i.uv);

				fixed4 shape = tex2D (_ShapeTex, i.uv);
				float powAlpha = pow(shape.a, 0.4);
				//clip(shape.a * noise.a - 0.2);
				if (shape.a < 0.6){
					if (shape.a * noise.a < 0.3){
						//refl = (pow(noise * (1 - shape.a * noise.a),2.2) * _EdgeColor + 0.2 * _EdgeColor + pow(noise, 100.0) * (1,0.5,1,0.1));
						refl = _EdgeColor + pow((1 - shape.a * noise.a), 2.2) * (1,0.5,1,1) * 0.5;
						refl.a = 0.5;
					} else {
						//refl = (pow(noise * (1 - shape.a * noise.a),2.2) * _EdgeColor + 0.2 * _EdgeColor + pow(noise, 100.0) * (1,0.5,1,0.1));
						refl = _InnerEdgeColor * (1 - powAlpha) + powAlpha * refl;
					}
					clip(shape.a * noise.a - 0.2);
				} else {
					refl = _InnerEdgeColor * (1 - powAlpha) + powAlpha * refl;
					clip(shape.a - 0.6);
				}
				return refl;
			}
			ENDCG
	    }
	}
	FallBack "Diffuse"
}
