﻿Shader "Custom/BoxFadeIn" {
	Properties {
		_Color ("Color", Color) = (1,1,1,1)
		_MainTex ("Albedo (RGB)", 2D) = "white" {}
		_FadeTex ("Fade (RGB)", 2D) = "white" {}
		_FadeColor ("Fade Emission (RGB)", 2D) = "white" {}
		_Glossiness ("Smoothness", Range(0,1)) = 0.5
		_Metallic ("Metallic", Range(0,1)) = 0.0
		_Threshold ("Treshold", Range(0,1)) = 0.0
	}
	SubShader {
		Tags { "Queue"="AlphaTest" "RenderType"="TransparentCutout" "IgnoreProjector"="True" }
		LOD 200
		
		CGPROGRAM
		// Physically based Standard lighting model, and enable shadows on all light types
		#pragma surface surf Standard fullforwardshadows

		// Use shader model 3.0 target, to get nicer looking lighting
		#pragma target 3.0

		sampler2D _MainTex;
		sampler2D _FadeTex;
		sampler2D _FadeColor;

		struct Input {
			float2 uv_MainTex;
		};

		half _Glossiness;
		half _Metallic;
		half _Threshold;
		fixed4 _Color;

		void surf (Input IN, inout SurfaceOutputStandard o) {
			// Albedo comes from a texture tinted by color
			fixed4 c = tex2D (_MainTex, IN.uv_MainTex) * _Color;
			fixed4 c2 = tex2D (_FadeTex, IN.uv_MainTex);
			fixed4 c3 = tex2D (_FadeColor, IN.uv_MainTex);
			float n = (c2.r + c2.g + c2.b)/3;
			if (n > _Threshold + 0.05f){
				o.Albedo = n;
				clip(-1);
			} else if (n > _Threshold - 0.05f){
				o.Albedo = c.r;
				o.Emission = c3.rgb;
			} else {
				o.Albedo = c.rgb;
			}
			//o.Emission = c.rgb;
			// Metallic and smoothness come from slider variables
			o.Metallic = _Metallic;
			o.Smoothness = _Glossiness;
			o.Alpha = c.a;
		}
		ENDCG
	}
	FallBack "Diffuse"
}
