﻿using UnityEngine;
using System.Collections;

// Custom gravity controller for an object that allows changing the gravity- and timescale for a single object
[RequireComponent (typeof(Rigidbody))]
public class CustomGravity : MonoBehaviour {

	[SerializeField] private float m_GravityMultiplier = 1.0f;      // Current gravity multiplier
	[SerializeField] private float m_Timescale = 1.0f;              // Current timescale
    [SerializeField] private AudioClip[] m_CollisionSounds;         // Sounds to make when colliding with other solid objects
	private Rigidbody m_Rigidbody;
    private AudioSource m_AudioSource;


	void Start(){
		m_Rigidbody = this.GetComponent<Rigidbody>();
        m_AudioSource = this.GetComponent<AudioSource>();
	}

	public void SetGravityScale(float scale) {      // Set the gravity scale to _scale_
		m_GravityMultiplier = scale;
	}

    public float GetGravityScale() {                // Get current gravity scale of the object
        return m_GravityMultiplier;
    }

    public float GetTimescale() {                   // Get current time scale of the object
        return m_Timescale;
    }

	public void SetTimescale(float scale) {         // Set the timescale of this object to _scale_
        // First reset the timescale
		m_Rigidbody.velocity /= m_Timescale;
		m_Rigidbody.angularVelocity /= m_Timescale;
		m_Rigidbody.mass *= m_Timescale;
		m_Rigidbody.drag /= m_Timescale;
		m_Rigidbody.angularDrag /= m_Timescale;

        // And apply the new scale
		m_Timescale = scale;

		m_Rigidbody.velocity *= scale;
		m_Rigidbody.angularVelocity *= scale;
		m_Rigidbody.mass /= scale;
		m_Rigidbody.drag *= m_Timescale;
		m_Rigidbody.angularDrag *= m_Timescale;
	}

	void FixedUpdate () {
		m_Rigidbody.AddForce(m_GravityMultiplier * Physics.gravity * m_Rigidbody.mass * m_Timescale);   // Move the object down (apply gravity)
	}

    void OnCollisionEnter(Collision col) {
        if (m_CollisionSounds.Length > 0) {
            if (col.relativeVelocity.magnitude > 0.5f && !m_AudioSource.isPlaying) {        // If the magnitude of the relative velocity of the collision is large enough and a collision sound is not already playing
                m_AudioSource.clip = m_CollisionSounds[Random.Range(0, m_CollisionSounds.Length)];  // Play a random collision sound from the list
                m_AudioSource.Play();
            }
        }
    }
}
