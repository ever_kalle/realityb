﻿using UnityEngine;
using System.Collections;

// Door that moves vertically
public class VerticalDoor : Triggerable {

	[SerializeField] private float m_Distance = 0.0f;   // How far up should the door move
	[SerializeField] private float m_Speed = 1.0f;      // How fast should the door move
    [SerializeField] private AudioClip m_MoveSound;     // The sound to make when the door moves
    [SerializeField] private Triggerable m_Target;      // A triggerable to trigger when the door opens/closes

	private float m_CurrentPos = 0.0f;      // Current position (0.0f - fully closed, 1.0f - fully open)
	private bool m_Open = false;            // Should the door be opening right now?
	private Vector3 m_ClosePos;             // Closed position in world space
	private Vector3 m_OpenPos;              // Opened position in world space

	private TimescaleController m_TimescaleController;
    private AudioSource m_AudioSource;

	void Start(){
		m_ClosePos = this.transform.position;       // Set the close position to starting position
		m_OpenPos = m_ClosePos + new Vector3(0.0f, m_Distance, 0.0f);   // Set the open position to closed postion + (m_Distance on y axis)

		m_TimescaleController = FindObjectOfType<TimescaleController>();    // Require a timescale controller in scene
		if (m_TimescaleController == null){
			Debug.LogError("Scene is missing a Timescale Controller");
		}

        m_AudioSource = this.GetComponent<AudioSource>();
	}

	void Update(){
		if (m_Open){                    // If the door should be opening now
			if (m_CurrentPos < 1.0f){   // And is not yet fully open
				m_CurrentPos += Time.deltaTime * m_Speed * m_TimescaleController.getTimescale();    // Increase the current position
			} else {
				m_CurrentPos = 1.0f;
                if (m_Target != null) {         // Once the door is fully open and it has a target
                    m_Target.TriggerStart();    // Trigger the target
                }
			}
		} else {                        // If the door should NOT be opening right now
			if (m_CurrentPos > 0.0f){   // and is not yet fully closed
				m_CurrentPos -= Time.deltaTime * m_Speed * m_TimescaleController.getTimescale(); // Decrease current position
			} else {
				m_CurrentPos = 0.0f;
                if (m_Target != null) {     // Once the door is fully closed and has a target
                    m_Target.TriggerEnd();  // end the trigger of the target
                }
			}
		}
		this.transform.position = Vector3.Lerp(m_ClosePos, m_OpenPos, m_CurrentPos);    // Set the doors position
	}

	public override void TriggerStart(){        // When the door is triggered
		m_Open = true;                          // it should start opening
        if (!m_AudioSource.isPlaying && m_CurrentPos < 1.0f) {      // And also play the moving sound
            m_AudioSource.clip = m_MoveSound;
            m_AudioSource.Play();
        }
	}

	public override void TriggerEnd(){          // When the trigger to the door ends
		m_Open = false;                         // the door should not be opening anymore
        if (!m_AudioSource.isPlaying && m_CurrentPos > 0.0f) {  // and should play the moving sound
            m_AudioSource.clip = m_MoveSound;
            m_AudioSource.Play();
        }
	}
}
