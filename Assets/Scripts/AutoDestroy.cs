﻿using UnityEngine;
using System.Collections;

public class AutoDestroy : MonoBehaviour {

    [SerializeField] private float m_Timer;     // Automatically destroys this object after m_Timer seconds

	void Update () {
        if (m_Timer > 0.0f) {
            m_Timer -= Time.deltaTime;
        } else {
            GameObject.Destroy(this.gameObject);
        }
	}
}
