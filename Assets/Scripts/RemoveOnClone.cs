﻿using UnityEngine;
using System.Collections;

// An object that should be destroyed on a clone object (during the reality transfer)
public class RemoveOnClone : MonoBehaviour {

	public void DestroyOnClone(){
		GameObject.Destroy(this.gameObject);
	}
}
