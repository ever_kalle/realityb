﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

// [obsolete, moved to ChangeReality.cs] Clones objects in portal area to show them in both realities
public class CloneObject : MonoBehaviour {

	private Dictionary<GameObject, GameObject> m_Clones = new Dictionary<GameObject, GameObject>();     // dict that holds objects and their clones

	void OnTriggerEnter(Collider other){
		if (other.gameObject.tag.Equals("PortalTeleport")){                             // if an object enters the portal area that can be moved between realities
			GameObject otherParent = other.gameObject.transform.parent.gameObject;      // get it's parent
			if (!otherParent.tag.Equals("Player")){                                     // if the parent is not player (we dont want to clone the player object)
				if (!m_Clones.ContainsKey(otherParent)){                                // and the parent does not have a clone yet
					GameObject clone = (GameObject)GameObject.Instantiate(otherParent, otherParent.transform.position, otherParent.transform.rotation); // create a clone of the object
					clone.BroadcastMessage("DestroyOnClone");           // destroy necessary objects on the clone
					clone.transform.SetParent(otherParent.transform);   // set clone's transform parent to the base object
					clone.layer = 0;                                    // set layer to 0, so it's visible in any reality
					Collider cloneCollider = clone.GetComponent<Collider>();    // disable collider on the clone if it exists
					if (cloneCollider != null){
						cloneCollider.enabled = false;
					}
					Rigidbody cloneRigidbody = clone.GetComponent<Rigidbody>();
                    if (cloneRigidbody != null) {                    // disable clone rigidbody collision detection
						cloneRigidbody.detectCollisions = false;
						cloneRigidbody.isKinematic = true;          // also make it kinematic so it follows it's parent around perfectly
					}
					m_Clones.Add(otherParent, clone);               // add the clone to the dictionary
				}
			}
		}
	}

	void OnTriggerExit(Collider other){
		if (other.gameObject.tag.Equals("PortalTeleport")){                         // if an object that can be moved between realities exits the portal area
			GameObject otherParent = other.gameObject.transform.parent.gameObject;
			if (m_Clones.ContainsKey(otherParent)){                             // if that object has a clone
				GameObject.Destroy(m_Clones[otherParent]);                      // destroy the clone
				m_Clones.Remove(otherParent);                                   // and remove the key
			}
		}
	}
}
