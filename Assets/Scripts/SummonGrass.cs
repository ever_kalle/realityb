﻿using UnityEngine;
using System.Collections;

// Creates foliage objects on this object
public class SummonGrass : MonoBehaviour {

    [SerializeField] private GameObject m_FoliagePrefab;                            // Prefab for foliage
    [SerializeField] private int m_MaxCount;                                        // Maximum number of foliage objects that can be created
    [SerializeField] private Material[] m_FoliageMaterials;                         // Materials that can be used by grass
    [SerializeField] private Material[] m_FlowerMaterials;                          // Materials that can be used by flowers
    [SerializeField] [Range(0.0f, 100.0f)] private float m_FlowerChance = 25.0f;    // Chance to create a flower instead of grass

	void Start () {
        int count = Random.Range(0, m_MaxCount * QualitySettings.GetQualityLevel() / 5);    // Create random number of foliage in range [0, m_MaxCount]. Also max count is dependant on the quality settigns ( so there would be less foliage on lower settings)
        int index;
        int index2;
        GameObject go;
        for (int i = 0; i < count; i++) {
            index = Random.Range(0, m_FoliageMaterials.Length);     // Select a random index for grass materials
            index2 = Random.Range(0, m_FlowerMaterials.Length);     // Select a random index for flower materials

            go = GameObject.Instantiate(m_FoliagePrefab);           // Create a foliage object...
            Vector3 foliagePosition = this.transform.position;
            foliagePosition.x += -2 + Random.Range(0.0f, 4.0f);
            foliagePosition.z += -2 + Random.Range(0.0f, 4.0f);
            go.transform.position = foliagePosition;                // ...and set it's position randomly within a range

            if (Random.Range(0.0f, 100.0f) < m_FlowerChance)    // Determine whether or not the created foliage object is grass or flowers
                go.GetComponent<Renderer>().material = m_FlowerMaterials[index2];   // Set the foliage object material to a random flower material
            else
                go.GetComponent<Renderer>().material = m_FoliageMaterials[index];   // Set the foliage object material to a random grass material
        }
	}
}
