﻿using UnityEngine;
using System.Collections;

// Pickup object for a reality cartridge
[RequireComponent(typeof(Collider))]
public class CartridgePickup : MonoBehaviour {

    [SerializeField] private int m_Reality;

    void OnTriggerEnter(Collider other) {
        string otherTag = other.gameObject.tag;
        if (otherTag.Equals("Player")) {        // If we collide with a player
            PortalDevice pd = other.GetComponentInChildren<PortalDevice>();     // Get the PortalDevice
            pd.EnableReality(m_Reality, true);                                  // and enable the reality
            GameObject.Destroy(this.gameObject);
        }
    }
}
