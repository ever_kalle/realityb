﻿using UnityEngine;
using System.Collections;

// Currently unused, sets a random threshold for a material cutoff
public class WallGlitch : MonoBehaviour {

    [SerializeField] private Material m_RespawnMaterial;
    private float m_Treshhold = 0.0f;

	void Start () {
        m_Treshhold = 0.3f;
        m_RespawnMaterial.SetFloat("_Threshold", m_Treshhold);
	}
	
	// Update is called once per frame
	void Update () {
        if (Random.Range(0.0f, 1.0f) > 0.97f) { 
	        m_Treshhold = Random.Range(0.25f, 0.77f);
            m_RespawnMaterial.SetFloat("_Threshold", m_Treshhold);
        }
	}
}
