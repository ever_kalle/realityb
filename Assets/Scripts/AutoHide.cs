﻿using UnityEngine;
using System.Collections;

public class AutoHide : MonoBehaviour {

	private float m_Timer = 0.0f;       // Automatically sets the object inactive after m_Timer seconds

	void Update () {
        if (m_Timer >= 0.0f) {
            m_Timer -= Time.deltaTime;
            if (m_Timer <= 0.0f) {
                this.gameObject.SetActive(false);
                m_Timer = 0.0f;
            }
        }
	}

    public void Hide(float n) {     // Set the object inactive after n seconds
        m_Timer = n;
    }
}
