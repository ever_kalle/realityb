﻿using UnityEngine;
using System.Collections;
using System;

[Serializable]
public class RealityPrefabs {

    [SerializeField] private GameObject[] m_Floors;

    [SerializeField] private GameObject[] m_Walls;

    [SerializeField] private GameObject m_Doorway;

    [SerializeField] private GameObject m_CornerInner;

    [SerializeField] private GameObject m_CornerOuter;

    [SerializeField] private GameObject m_Light;

    // Get the floor object of this reality
    public GameObject getFloor() {
        return m_Floors[UnityEngine.Random.Range(0, m_Floors.Length)];
    }

    // Get the wall object of this reality
    public GameObject getWall() {
        return m_Walls[UnityEngine.Random.Range(0, m_Walls.Length)];
    }

    // Get the doorway object of this reality
    public GameObject getDoorway() {
        return m_Doorway;
    }

    // Get the inner corner object of this reality
    public GameObject getCornerInner() {
        return m_CornerInner;
    }

    // Get the outer corner object of this reality
    public GameObject getCornerOuter() {
        return m_CornerOuter;
    }

    // Get the light object of this reality
    public GameObject getLight() {
        return m_Light;
    }
}
