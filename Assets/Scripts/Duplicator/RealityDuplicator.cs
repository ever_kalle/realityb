﻿using UnityEngine;
using System.Collections;

// Used in editor: duplicates the prototype level to each reality
public class RealityDuplicator : MonoBehaviour {

    [SerializeField] private string[] m_BaseObjectNames;

    [SerializeField] private RealityPrefabs m_RealityA = new RealityPrefabs();          // Prefabs used by each reality
    [SerializeField] private RealityPrefabs m_RealityB = new RealityPrefabs();
    [SerializeField] private RealityPrefabs m_Reality0g = new RealityPrefabs();
    [SerializeField] private RealityPrefabs m_RealitySlow = new RealityPrefabs();
    [SerializeField] private RealityPrefabs m_RealityLiving = new RealityPrefabs();
	
    // Duplicate objects
    public void Duplicate() {
        // Create parent objects for each reality;
        GameObject parentA = new GameObject("RealityA_Duplicated");
        parentA.layer = LayerMask.NameToLayer("RealityA");
        parentA.tag = "Reality";

        GameObject parentB = new GameObject("RealityB_Duplicated");
        parentB.layer = LayerMask.NameToLayer("RealityB");
        parentB.tag = "Reality";

        GameObject parent0g = new GameObject("Reality0g_Duplicated");
        parent0g.layer = LayerMask.NameToLayer("Reality0g");
        parent0g.tag = "Reality";

        GameObject parentSlow = new GameObject("RealitySlow_Duplicated");
        parentSlow.layer = LayerMask.NameToLayer("RealitySlow");
        parentSlow.tag = "Reality";

        GameObject parentLiving = new GameObject("RealityLiving_Duplicated");
        parentLiving.layer = LayerMask.NameToLayer("RealityLiving");
        parentLiving.tag = "Reality";

        GameObject[] toReplace = GameObject.FindGameObjectsWithTag("ToBeReplaced"); // Find all prototype objects
        for (int i = 0; i < toReplace.Length; i++) {
            if (toReplace[i].name.Contains("floor")) {                              // Duplicate floors
                CreateDuplicate(m_RealityA.getFloor(), toReplace[i], parentA);
                CreateDuplicate(m_RealityB.getFloor(), toReplace[i], parentB);
                CreateDuplicate(m_Reality0g.getFloor(), toReplace[i], parent0g);
                CreateDuplicate(m_RealitySlow.getFloor(), toReplace[i], parentSlow);
                CreateDuplicate(m_RealityLiving.getFloor(), toReplace[i], parentLiving);
            } else if (toReplace[i].name.Contains("inner")) {                       // Duplicate inner corners
                CreateDuplicate(m_RealityA.getCornerInner(), toReplace[i], parentA);
                CreateDuplicate(m_RealityB.getCornerInner(), toReplace[i], parentB);
                CreateDuplicate(m_Reality0g.getCornerInner(), toReplace[i], parent0g);
                CreateDuplicate(m_RealitySlow.getCornerInner(), toReplace[i], parentSlow);
                CreateDuplicate(m_RealityLiving.getCornerInner(), toReplace[i], parentLiving);
            } else if (toReplace[i].name.Contains("outer")) {                       // Duplicate outer corners
                CreateDuplicate(m_RealityA.getCornerOuter(), toReplace[i], parentA);
                CreateDuplicate(m_RealityB.getCornerOuter(), toReplace[i], parentB);
                CreateDuplicate(m_Reality0g.getCornerOuter(), toReplace[i], parent0g);
                CreateDuplicate(m_RealitySlow.getCornerOuter(), toReplace[i], parentSlow);
                CreateDuplicate(m_RealityLiving.getCornerOuter(), toReplace[i], parentLiving);
            } else if (toReplace[i].name.Contains("wall")) {                        // Duplicate walls
                CreateDuplicate(m_RealityA.getWall(), toReplace[i], parentA);
                CreateDuplicate(m_RealityB.getWall(), toReplace[i], parentB);
                CreateDuplicate(m_Reality0g.getWall(), toReplace[i], parent0g);
                CreateDuplicate(m_RealitySlow.getWall(), toReplace[i], parentSlow);
                CreateDuplicate(m_RealityLiving.getWall(), toReplace[i], parentLiving);
            } else if (toReplace[i].name.Contains("doorway")) {                     // Duplicate doorways
                CreateDuplicate(m_RealityA.getDoorway(), toReplace[i], parentA);
                CreateDuplicate(m_RealityB.getDoorway(), toReplace[i], parentB);
                CreateDuplicate(m_Reality0g.getDoorway(), toReplace[i], parent0g);
                CreateDuplicate(m_RealitySlow.getDoorway(), toReplace[i], parentSlow);
                CreateDuplicate(m_RealityLiving.getDoorway(), toReplace[i], parentLiving);
            } else if (toReplace[i].name.Contains("Light")) {                       // Duplicate lights
                CreateDuplicate(m_RealityA.getLight(), toReplace[i], parentA);
                CreateDuplicate(m_RealityB.getLight(), toReplace[i], parentB);
                CreateDuplicate(m_Reality0g.getLight(), toReplace[i], parent0g);
                CreateDuplicate(m_RealitySlow.getLight(), toReplace[i], parentSlow);
                CreateDuplicate(m_RealityLiving.getLight(), toReplace[i], parentLiving);
            }
        }
    }

    // Destroy duplicates
    public void RemoveDuplicates() {
        GameObject g = GameObject.Find("RealityA_Duplicated");  // Find the duplicated Reality A object
        if (g != null)
            GameObject.DestroyImmediate(g);                     // and destroy it if it exists
        g = GameObject.Find("RealityB_Duplicated");             // Do the same with other realities
        if (g != null)
            GameObject.DestroyImmediate(g);
        g = GameObject.Find("Reality0g_Duplicated");
        if (g != null)
            GameObject.DestroyImmediate(g);
        g = GameObject.Find("RealitySlow_Duplicated");
        if (g != null)
            GameObject.DestroyImmediate(g);
        g = GameObject.Find("RealityLiving_Duplicated");
        if (g != null)
            GameObject.DestroyImmediate(g);
    }

    /**
     * Creates a duplicate object
     * 
     * prefab - the prefab to instantiate
     * orig - the prototype object
     * parent - the parent to set for the instantiated prefab
     **/
    private void CreateDuplicate(GameObject prefab, GameObject orig, GameObject parent) {
        GameObject g = GameObject.Instantiate(prefab);
        g.transform.position = orig.transform.position;
        g.transform.rotation = orig.transform.rotation;
        g.transform.localScale = orig.transform.localScale;
        g.transform.SetParent(parent.transform);
    }
}
