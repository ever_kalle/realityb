﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

// Changes the reality of the objects that enter the portal area
public class ChangeReality : MonoBehaviour {

	[SerializeField] private LayerMask m_NewLayers = -1;
	[SerializeField] private int m_NewLayer = -1;
	[SerializeField] private int newReality = 0;

	private int m_LayerShift = 10;
	private int m_OldLayer;
	private int m_OldReality;
	private LayerMask m_PreviousLayermask;
    private Dictionary<GameObject, GameObject> m_Clones = new Dictionary<GameObject, GameObject>();    // dict that holds objects and their clones

	private TimescaleController m_TimescaleController;

	void Start(){
		m_TimescaleController = FindObjectOfType<TimescaleController>();
		if (m_TimescaleController == null){
			Debug.LogError("Scene is missing a Timescale Controller");
		}
	}

	void OnTriggerEnter(Collider other){
        if (other.gameObject.tag.Equals("PortalTeleport")) {                                // if an object enters the portal area that can be moved between realities
            GameObject otherParent = other.gameObject.transform.parent.gameObject;          // get it's parent
            if (!otherParent.tag.Equals("Player")) {                                        // if the parent is not player (we dont want to clone the player object)
                if (!m_Clones.ContainsKey(otherParent)) {                                   // and the parent does not have a clone yet
                    GameObject clone = (GameObject)GameObject.Instantiate(otherParent, otherParent.transform.position, otherParent.transform.rotation); // create a clone of the object
                    clone.BroadcastMessage("DestroyOnClone");           // destroy necessary objects on the clone
                    clone.transform.SetParent(otherParent.transform);   // set clone's transform parent to the base object
                    clone.layer = 0;                                    // set layer to 0, so it's visible in any reality
                    Collider cloneCollider = clone.GetComponent<Collider>();    // disable collider on the clone if it exists
					if (cloneCollider != null){
						cloneCollider.enabled = false;
					}
					Rigidbody cloneRigidbody = clone.GetComponent<Rigidbody>();
					if (cloneRigidbody != null){
                        cloneRigidbody.detectCollisions = false;    // disable clone rigidbody collision detection
                        cloneRigidbody.isKinematic = true;          // also make it kinematic so it follows it's parent around perfectly
					}
                    m_Clones.Add(otherParent, clone);               // add the clone to the dictionary
				}
			}
		}
	}

	void OnTriggerExit(Collider other){
        if (other.gameObject.tag.Equals("PortalTeleport")) {                        // if an object that can be moved between realities exits the portal area
			GameObject otherParent = other.gameObject.transform.parent.gameObject;
            if (m_Clones.ContainsKey(otherParent)) {                                    // if that object has a clone
                GameObject.Destroy(m_Clones[otherParent]);                              // destroy the clone
                m_Clones.Remove(otherParent);                                           // and remove the entry from dict
			}

			Vector3 relativeVector = other.transform.position - this.transform.position;    // get the vector between portal and object
			float angle = Vector3.Angle(relativeVector, this.transform.up);                 // get the angle between relative vector and normal

			if (angle > 95.0f){                 // if the object is behind portal
				other.gameObject.layer = m_NewLayer;    // move it to a new layer

				Rigidbody playerRigidbody = other.GetComponentInParent<Rigidbody>();    // get it's rigidbody
				playerRigidbody.gameObject.layer = m_NewLayer;

				Camera camera = playerRigidbody.GetComponentInChildren<Camera>();       // get camera (if it exists)

				if (newReality == 2){                   // if the object moved into reality 0g, disable it's gravity
					playerRigidbody.gameObject.BroadcastMessage("SetGravityScale", 0.0f);
				} else {
					playerRigidbody.gameObject.BroadcastMessage("SetGravityScale", 1.0f);   // otherwise enable the gravity
				}

				if (camera != null){                        //if the object has a camera (so is a player)
					camera.cullingMask = m_NewLayers.value; // set the camera to render new reality

					PortalDevice portalDevice = playerRigidbody.GetComponentInChildren<PortalDevice>();

					GameObject go = GameObject.Instantiate(portalDevice.GetCurrentPortalPrefab()); // create a portal to previous reality
					go.transform.position = this.transform.position;                               // at the same position as this portal
					go.transform.rotation = this.transform.rotation;
					go.transform.Rotate(180.0f,0.0f,0.0f);                                         // rotated 180 degrees
                    ChangeReality changeReality = go.GetComponent<ChangeReality>();
                    changeReality.SetOldReality(newReality);                                        // set it's reality

					portalDevice.SetOpenPortal(go);         // update the open portal of the portal device

					portalDevice.SetReality(newReality);        // update the current reality of the portal device

					if (newReality == 3) {                  // if player entered slow reality
						m_TimescaleController.slowDown();   // slow the time down
					} else {
						m_TimescaleController.speedUp();    // otherwise set the time scale to normal
					}

					GameObject.Destroy(this.gameObject);        // destroy the old portal
				} else {                                                                    // collision with a box
                    CanPickUp cpu = other.GetComponentInParent<CanPickUp>();
                    if (cpu != null) {
                        cpu.EnableRealityObject(newReality);    // show the proper ghost box for the new reality
                    }

					if (newReality == 3) {                      // if the box's new reality is slow, then slow it down
						playerRigidbody.gameObject.BroadcastMessage("SetTimescale", m_TimescaleController.getSlowTimeConst());
					} else {
						playerRigidbody.gameObject.BroadcastMessage("SetTimescale", 1.0f);  // otherwise set it's timescale to normal
					}
				}
			} else if (angle < 85.0f) {     // if the object is in front of the portal, then do the same stuff as previously, but only if it's a box to transfer it "back" through the portal
				other.gameObject.layer = m_OldLayer;

				Rigidbody playerRigidbody = other.GetComponentInParent<Rigidbody>();
				playerRigidbody.gameObject.layer = m_OldLayer;

                CanPickUp cpu = other.GetComponentInParent<CanPickUp>();
                if (cpu != null) {
                    cpu.EnableRealityObject(m_OldReality);
                }

				if (m_OldReality == 2){
					playerRigidbody.gameObject.BroadcastMessage("SetGravityScale", 0.0f);
				} else {
					playerRigidbody.gameObject.BroadcastMessage("SetGravityScale", 1.0f);
				}

                if (cpu != null) {
                    if (m_OldReality == 3) {
                        playerRigidbody.gameObject.BroadcastMessage("SetTimescale", m_TimescaleController.getSlowTimeConst());
                    } else {
                        playerRigidbody.gameObject.BroadcastMessage("SetTimescale", 1.0f);
                    }
                }
			}
		}
	}

	public void SetOldReality(int reality){     // sets the previous reality of the portal (so the reality in which the portal is)
		m_OldReality = reality;
		m_OldLayer = reality + m_LayerShift;
	}

    public int GetNewRealityLayer() {
        return m_NewLayer;
    }

	void OnDestroy(){
		foreach(GameObject clone in m_Clones.Values)    // destroy all clones if the portal is destroyed.
		{
			GameObject.Destroy(clone);
		}
	}
}
