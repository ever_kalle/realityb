﻿using UnityEngine;
using System.Collections;
using UnityEditor;

[CustomEditor(typeof(RealityDuplicator))]
public class RealityDuplicatorEditor : Editor {

    public override void OnInspectorGUI() {
        DrawDefaultInspector();

        RealityDuplicator myScript = (RealityDuplicator)target;
        if (GUILayout.Button("Duplicate")) {
            if (EditorUtility.DisplayDialog("Create duplicates?", "U sure u wanna duplicate? (could b slow)", "Yea", "Nah")) {
                myScript.Duplicate();
            }
        }

        if (GUILayout.Button("Remove Duplicates")) {
            if (EditorUtility.DisplayDialog("Remove duplicates?", "U sure u wanna remove? (could destroy progress)", "Yea", "Nah")) {
                myScript.RemoveDuplicates();
            }
        }
    }
}
