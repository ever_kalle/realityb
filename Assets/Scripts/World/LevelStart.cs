﻿using UnityEngine;
using System.Collections;

// starts a level and places player in the correct location
public class LevelStart : MonoBehaviour {

    public string m_TargetLevel = "s1-1";
    [SerializeField] GameObject m_LoadingScreen;

	// Use this for initialization
	void Start () {
	    GameObject[] gos = GameObject.FindGameObjectsWithTag("Area");
        LevelLoader[] loaders = GameObject.FindObjectsOfType<LevelLoader>();
        PortalDevice pd = GameObject.FindObjectOfType<PortalDevice>();
        LevelLoader ll = null;

        MenuController menuController = GameObject.FindObjectOfType<MenuController>();
        if (menuController != null) {
            m_TargetLevel = menuController.GetLevel();
            menuController.SetLoadingScreen(m_LoadingScreen);
        }

        GameObject toLoad = null;
        foreach (LevelLoader loader in loaders){
            if (loader.GetName().Equals(m_TargetLevel)) {   // find the correct area in which to place the player
                toLoad = loader.GetCurrent();
                ll = loader;
            }
        }

        foreach (GameObject go in gos) {
            go.SetActive(false);
        }

        toLoad.SetActive(true);
        pd.SetRespawnPos(ll);
        pd.Respawn(false);      // respawn the player to move them to the proper level load zone
	}

    void OnLevelWasLoaded() { // same as Start()
        GameObject[] gos = GameObject.FindGameObjectsWithTag("Area");
        LevelLoader[] loaders = GameObject.FindObjectsOfType<LevelLoader>();
        PortalDevice pd = GameObject.FindObjectOfType<PortalDevice>();
        LevelLoader ll = null;

        MenuController menuController = GameObject.FindObjectOfType<MenuController>();
        if (menuController != null) {
            m_TargetLevel = menuController.GetLevel();
        }

        GameObject toLoad = null;
        foreach (LevelLoader loader in loaders) {
            if (loader.GetName().Equals(m_TargetLevel)) {
                toLoad = loader.GetCurrent();
                ll = loader;
            }
        }

        foreach (GameObject go in gos) {
            go.SetActive(false);
        }

        toLoad.SetActive(true);
        pd.SetRespawnPos(ll);
        pd.Respawn(false);
    }
}
