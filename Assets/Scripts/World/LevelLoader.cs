﻿using UnityEngine;
using System.Collections;

// Loads new areas and unloads previous ones
[RequireComponent(typeof(Collider))]
public class LevelLoader : MonoBehaviour {

    [SerializeField] private string m_StageName;        // this area's name
    [SerializeField] private GameObject m_ToUnload;     // area to unload
    [SerializeField] private GameObject m_ToLoad;       // area to load
    [SerializeField] private GameObject m_Current;      // current area parent object
    [SerializeField] private GameObject m_Next;         // next area
    [SerializeField] private LevelEndDoor m_Door;       // the level end door attached to this level loader
    [SerializeField] private LevelLoader m_TeleportTarget;  // level loader to which the player should be teleported (can be null for no teleporting)
    [SerializeField] private bool[] m_EnabledRealities = {true, true, false, false, false};     // what realities should be enabled when loading a game to this area
    [SerializeField] private GameObject m_BeamPrefab;       // box destroying beams prefabs
    [SerializeField] private CanPickUp[] m_IgnoredBoxes;    // boxes to NOT destroy when they enter the load zone
    [SerializeField] private string m_NewWorldNum;          // if not empty loads a level with this number

    private bool m_Activated = false;   // has the player entered this load zone

    public GameObject GetCurrent() {    // returns current area parent object
        return m_Current;
    }

    public string GetName() {           // returns this areaes name
        return m_StageName;
    }

    void OnTriggerEnter(Collider other) {
        string otherTag = other.gameObject.tag;
        if (otherTag.Equals("Player")) {
            if (m_TeleportTarget != null) { // if we have a teleport target 
                Vector3 relativePlayerPos = other.transform.position - this.transform.position; // move player to it's position
                other.transform.position = m_TeleportTarget.transform.position + relativePlayerPos;
            } else if (!m_NewWorldNum.Equals("")) { // if we should load a new scene
                FindObjectOfType<MenuController>().StartLevel("s" + m_NewWorldNum + "-1"); // start a new level
            } else {
                if (!m_Activated) {
                    m_Door.CloseDoor(m_ToUnload);   // close the level end doro
                    m_ToLoad.SetActive(true);       // set a new area active
                    m_Next.SetActive(true);
                    m_Activated = true;

                    PortalDevice pd = other.GetComponentInChildren<PortalDevice>();
                    pd.SetRespawnPos(this);
                    pd.ClosePortal();

                    /**
                     * Enable realities if necessary
                     */
                    if (m_EnabledRealities[1]) {
                        if (!pd.isRealityEnabled(1)) {
                            pd.ReceiveDevice(false);
                        }
                    }
                    if (m_EnabledRealities[2]) {
                        pd.EnableReality(2, false);
                    }
                    if (m_EnabledRealities[3]) {
                        pd.EnableReality(3, false);
                    }
                    if (m_EnabledRealities[4]) {
                        pd.EnableReality(4, false);
                    }

                    string s = m_StageName.Replace("s", "");
                    string[] nums = s.Split('-');
                    pd.UpdateCurrentArea(int.Parse(nums[0]), int.Parse(nums[1]));
                    if (GameObject.FindObjectOfType<MenuController>() != null)                  
                        MenuController.UnlockLevel(int.Parse(nums[0]), int.Parse(nums[1]));     // unlock this area for selection in main menu
                }
            }
        }
        CanPickUp pickupObject = other.gameObject.GetComponent<CanPickUp>();
        if (pickupObject != null) { // destroy boxes on collision if they are not ignored
            bool destroy = true;
            foreach (CanPickUp cpu in m_IgnoredBoxes) {
                if (cpu == pickupObject) {
                    destroy = false;
                    break;
                }
            }
            if (destroy) {
                GameObject go = GameObject.Instantiate(m_BeamPrefab);  // create laser beams to indicate the destruction of box
                go.transform.position = this.transform.position + new Vector3(0.0f, -1.863f, 0.0f);
                go.transform.LookAt(pickupObject.transform);
                go.transform.localScale = new Vector3(1.0f, 1.0f, Mathf.Abs((this.transform.position + new Vector3(0.0f, -1.863f, 0.0f) - pickupObject.transform.position).magnitude));
                go = GameObject.Instantiate(m_BeamPrefab);
                go.transform.position = this.transform.position + new Vector3(0.0f, 1.863f, 0.0f);
                go.transform.LookAt(pickupObject.transform);
                go.transform.localScale = new Vector3(1.0f, 1.0f, Mathf.Abs((this.transform.position + new Vector3(0.0f, 1.863f, 0.0f) - pickupObject.transform.position).magnitude));
                pickupObject.DestroyBox();
            }
        }
    }
}
