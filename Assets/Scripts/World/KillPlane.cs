﻿using UnityEngine;
using System.Collections;

// Makes the player and box respawn on collision
[RequireComponent(typeof(Collider))]
public class KillPlane : MonoBehaviour {

    void OnTriggerEnter(Collider other) {
        string otherTag = other.gameObject.tag;
        if (otherTag.Equals("Player")) {
            PortalDevice pd = other.GetComponentInChildren<PortalDevice>();
            pd.Respawn(true); // respawn player on collision
            other.BroadcastMessage("StopMovement");
        } else {
            CanPickUp cpu = other.GetComponentInParent<CanPickUp>();
            if (cpu != null) {
                cpu.DestroyBox();   // respawn a box on collision
            }
        }
    }
}
