﻿using UnityEngine;
using System.Collections;

// door used at each area end
public class LevelEndDoor : MonoBehaviour {

    [SerializeField] private float m_Distance = 0.0f;   // how far the door moves
    [SerializeField] private float m_Speed = 1.0f;      // how fast it moves
    private GameObject m_ToUnload = null;               // area to unload after the door closes

    private Vector3 m_ClosePos;
    private Vector3 m_OpenPos;

    private bool m_Closing = false;
    private float m_CurrentPos = 1.0f;
    private bool m_Deactivated = false; // have we unloaded the previous area
    private AudioSource m_AudioSource;

    void Start() {
        m_ClosePos = this.transform.position;
        m_OpenPos = m_ClosePos + new Vector3(0.0f, m_Distance, 0.0f);
        this.transform.position = m_OpenPos;
        m_AudioSource = this.GetComponent<AudioSource>();
    }
	
	void Update () {
        if (m_Closing) {                            // if the door should be closing
            if (m_CurrentPos > 0.0f) {              // and is not currently closed
                m_CurrentPos -= Time.deltaTime * m_Speed;   // close it
            } else {
                m_CurrentPos = 0.0f;    //when the door is fully closed
                if (!m_Deactivated) {   // and the previous area has not yet been unloaded
                    m_AudioSource.Play();   // play door closing sound
                    m_ToUnload.SetActive(false);    // deactivate previous area
                    m_Deactivated = true;
                }
            }
            this.transform.position = Vector3.Lerp(m_ClosePos, m_OpenPos, m_CurrentPos);
        }
	}

    public void CloseDoor(GameObject toUnload) {    // start closing the door and set the area that should be unloaded to @toUnload
        m_Closing = true;
        m_ToUnload = toUnload;
    }
}
