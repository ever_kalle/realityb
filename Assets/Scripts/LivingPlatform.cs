﻿using UnityEngine;
using System.Collections;

// Platform that moves vertically in living reality
public class LivingPlatform : MonoBehaviour {

	public float m_Distance = 0.0f;     // The distance to move on y axis
	public float m_Speed = 1.0f;        // The speed to move at

	private float m_CurrentPos = 0.0f;  // Current position in movement (0.0f - one end of movement, 1.0f - other end)
	private bool m_MovingUp = true;     // Is this object currently moving up
	private Vector3 m_DownPos;          // The down position of the movement path
	private Vector3 m_UpPos;            // upper position

	private GameObject m_MoveTarget = null;
	private Vector3 m_TargetOffset;

	void Start(){
		m_DownPos = this.transform.position;
		m_UpPos = m_DownPos + new Vector3(0.0f, m_Distance, 0.0f);
	}

	void Update(){
		if (m_MovingUp){                // If the platform is moving up
			if (m_CurrentPos < 1.0f){   // and has not yet reached the upper position
				m_CurrentPos += Time.deltaTime * m_Speed;   // increase the position
			} else {                    // otherwise reverse the direction
				m_CurrentPos = 1.0f;
				m_MovingUp = false;
			}
		} else {                        // same thing for moving down
			if (m_CurrentPos > 0.0f){
				m_CurrentPos -= Time.deltaTime * m_Speed;
			} else {
				m_CurrentPos = 0.0f;
				m_MovingUp = true;
			}
		}
		this.transform.position = Vector3.Lerp(m_DownPos, m_UpPos, m_CurrentPos);   // set the object position
	}

	//http://answers.unity3d.com/questions/12083/how-to-get-a-character-to-move-with-a-moving-platf.html
	void OnTriggerStay(Collider other){
		if (other.gameObject.tag.Equals("Player")){ // If player remains on the platform
			m_MoveTarget = other.gameObject;            // Set it as a move target
			m_TargetOffset = m_MoveTarget.transform.position - transform.position; // and get the offset between this position and player position
		}

	}

	void OnTriggerExit(Collider other){
		if (other.gameObject.tag.Equals("Player")){ // if player steps off the platform
			m_MoveTarget = null;
		}
	}

	void LateUpdate(){
		if (m_MoveTarget != null) {
			m_MoveTarget.transform.position = transform.position + m_TargetOffset; // Move the player with platform
		}
	}
}
