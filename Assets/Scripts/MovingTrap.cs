﻿using UnityEngine;
using System.Collections;

// A trap that moves between two endpoints
public class MovingTrap : MonoBehaviour {

    [SerializeField] float m_Distance;      // distance to move
    [SerializeField] float m_Acceleration;  // acceleration when the trap is in the active state
    [SerializeField] float m_RetractSpeed;  // the speed at which to retract the trap
    [SerializeField] Vector3 m_MoveDir;     // direction in which the trap moves
    [SerializeField] float m_WaitTimeAtLethal;  // how long does the trap wait in the lethal end of movement before retracting
    [SerializeField] float m_WaitTimeAtSafe;    // how long does the trap wait in the retracted state
    [SerializeField] private AudioClip m_MoveSound;     // sound to make when the trap moves
    [SerializeField] private AudioClip m_CloseSound;    // sound to make when the trap reaches it's lethal state

    private float m_CurrentPos = 0.0f;      // current position of the trap (0.0f - retracted, 1.0f - lethal)
    private float m_Speed = 0.0f;           // current movement speed
    private Vector3 m_UpPos;                // retracted position
    private Vector3 m_DownPos;              // lethal position
    private bool m_MovingDown;              // is the trap currently moving towards the lethal position
    private float m_WaitTime = 0.0f;        // timer for waiting at one end of movement
    private float m_CurrentRetractSpeed = 0.0f;

    private TimescaleController m_TimescaleController;
    private AudioSource m_AudioSource;

    void Start() {
        m_UpPos = this.transform.position;              // set the retracted
        m_DownPos = m_UpPos + m_MoveDir * m_Distance;   // and lethal positions

        m_TimescaleController = FindObjectOfType<TimescaleController>();
        if (m_TimescaleController == null) {
            Debug.LogError("Scene is missing a Timescale Controller");
        }

        m_AudioSource = this.GetComponent<AudioSource>();
    }

    void Update() {
        if (m_WaitTime == 0.0f) {               // if we are currently not waiting
            if (m_MovingDown) {                     // if moving towards the lethal position
                if (m_CurrentPos < 1.0f) {          // and have not yet reached it
                    m_Speed += m_Acceleration * m_TimescaleController.getTimescale();       // increase movement speed by acceleration
                    m_CurrentPos += Time.deltaTime * m_Speed * m_TimescaleController.getTimescale();    // increase position by speed
                } else {                // reached lethal position
                    m_Speed = 0.0f;     // set speed to 0
                    m_CurrentPos = 1.0f;
                    m_MovingDown = false;   // set state to retracting
                    m_WaitTime = m_WaitTimeAtLethal;    // wait for m_WaitTimeAtLethal seconds
                    m_AudioSource.clip = m_CloseSound;  // play the sound
                    m_AudioSource.Play();
                }
            } else {    // if the trap is currently retracting
                if (m_CurrentPos > 0.0f) {      // not yet fully retracted
                    m_CurrentRetractSpeed += 0.1f * m_Acceleration * m_TimescaleController.getTimescale();  // increase retracting speed
                    m_CurrentRetractSpeed = Mathf.Clamp(m_CurrentRetractSpeed, 0.0f, m_RetractSpeed);       // clamp the retracting speed
                    m_CurrentPos -= Time.deltaTime * m_CurrentRetractSpeed * m_TimescaleController.getTimescale();  // and reduce current position by retracting speed
                } else {
                    m_CurrentRetractSpeed = 0.0f;   // set retracting speed to 0
                    m_Speed = 0.0f;
                    m_CurrentPos = 0.0f;
                    m_MovingDown = true;    // set state to lethal
                    m_WaitTime = m_WaitTimeAtSafe;  // wait for m_WaitTimeAtSafe seconds
                }
            }
        } else {    // trap is currently waiting at one endpoint
            m_WaitTime -= Time.deltaTime * m_TimescaleController.getTimescale();
            if (m_WaitTime <= 0.0f) {
                m_WaitTime = 0.0f;
                if (m_CurrentPos >= 0.5f) {     // if we are retracting
                    m_AudioSource.clip = m_MoveSound;   // play move sound
                    m_AudioSource.Play();
                }
            }
        }
        this.transform.position = Vector3.Lerp(m_UpPos, m_DownPos, m_CurrentPos);   // set trap position
    }
}
