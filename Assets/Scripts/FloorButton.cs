﻿using UnityEngine;
using System.Collections;

// Button on the floor
[RequireComponent (typeof(Collider))]
public class FloorButton : MonoBehaviour {

	[SerializeField] private Triggerable m_Target;          // A triggerable to be triggered by this button
	[SerializeField] private Transform m_ButtonBase;        // the button part to move
    [SerializeField] private float m_TimerLength;           // how long does the trigger last
    [SerializeField] private AudioSource m_AudioSource;     // Audio source for button pressing/releasing
    [SerializeField] private AudioSource m_TickingSource;   // Audio source for the ticking sound of timed button

	private Vector3 m_ButtonStartPos;       
	private Vector3 m_ButtonEndPos;
	private int m_CurrentCollisions = 0;    // how many collisions we have currently with objects that can trigger us
	private float m_ButtonPosition = 0.0f;  // position of the button

	private TimescaleController m_TimescaleController;
    private bool m_Pressed = false;     // is the button pressed right now
    private float m_Timer;              // Timer to end the trigger on timed button
    private float m_TickTimer = 0.0f;   // Timer for the ticking noise

	void Start () {
		m_ButtonStartPos = m_ButtonBase.localPosition;                          // set the button movement positions
		m_ButtonEndPos = m_ButtonStartPos + new Vector3(0.0f, -0.8f, 0.0f);     // .

		m_TimescaleController = FindObjectOfType<TimescaleController>();    // require a timescale controller
		if (m_TimescaleController == null){
			Debug.LogError("Scene is missing a Timescale Controller");
		}
	}

	void Update () {
		if (m_CurrentCollisions > 0){           // If we have more than one collision
			if (m_ButtonPosition < 1.0f){       // Move the button down
				m_ButtonPosition += Time.deltaTime * 2.0f * m_TimescaleController.getTimescale();
			} else {
				m_ButtonPosition = 1.0f;
			}
		} else {                                // otherwise
			if (m_ButtonPosition > 0.0f){       // move the button up
				m_ButtonPosition -= Time.deltaTime * 2.0f * m_TimescaleController.getTimescale();
			} else {
				m_ButtonPosition = 0.0f;
			}
		}
		m_ButtonBase.localPosition = Vector3.Lerp(m_ButtonStartPos, m_ButtonEndPos, m_ButtonPosition);
        if (m_Timer > 0.0f) {   // If it's  timed button
            m_Timer -= Time.deltaTime * m_TimescaleController.getTimescale();   // decrease the timer
            if (m_TickTimer > 0.0f) {   // if ticking timer is larger than 0
                m_TickTimer -= Time.deltaTime * m_TimescaleController.getTimescale();   // reduce the ticking timer
                if (m_TickTimer <= 0.0f) {
                    m_TickingSource.Play();     // and play the ticking sound when the ticking timer reaches 0
                    if (m_Timer >= 0.75f) {     // if there is enough time left on the trigger timer, start another ticking sound loop
                        m_TickTimer = 1.0f;
                    }
                }
            }
            if (m_Timer <= 0) {             // once the trigger timer reaches 0
                m_Target.TriggerEnd();      // end the trigger
            }
        }
	}

	void OnTriggerEnter(Collider other) {
		string otherTag = other.gameObject.tag;
		CanPickUp pickupObject = other.gameObject.GetComponent<CanPickUp>();
		if (otherTag.Equals("Player") || pickupObject != null) {        // if we start colliding with either a box or a player
			m_CurrentCollisions += 1;                                   // increase the number of current collisions
		}

		if (m_CurrentCollisions > 0){           // if we currently have more than 0 collisions
            m_Timer = 0.0f;
			m_Target.TriggerStart();            // start the trigger
            if (!m_Pressed) {
                m_AudioSource.Play();       // and play the sound if button was not previously triggered
                m_Pressed = true;
            }
		}
	}

	void OnTriggerExit(Collider other) {
		string otherTag = other.gameObject.tag;
		CanPickUp pickupObject = other.gameObject.GetComponent<CanPickUp>();
		if (otherTag.Equals("Player") || pickupObject != null) {        // if a collision with player/box ends
			m_CurrentCollisions -= 1;                                   // reduce the collision count
		}

		if (m_CurrentCollisions == 0){          // if there are no current collisions
            if (m_TimerLength == 0)     // if the button is not timed
			    m_Target.TriggerEnd();  // end the trigger
            else { 
                m_Timer = m_TimerLength + 0.01f;    // otherwise start the trigger timer
                m_TickTimer = 1.0f;                 // and ticking timer
            }
            if (m_Pressed) {            // if the button was previously pressed
                m_AudioSource.Play();   // play the button sound
                m_Pressed = false;
            }
		}
	}
}
