﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityStandardAssets.Characters.FirstPerson;
using UnityEngine.UI;

[RequireComponent(typeof(Camera))]
[RequireComponent(typeof(AudioSource))]
public class PortalDevice : MonoBehaviour {

	[SerializeField] private GameObject[] m_Portals;            // portal prefabs
	[SerializeField] private Transform m_Hand;                  // hand base
	[SerializeField] private Material m_DeviceMaterial;         // material of the portal device (to change it's emissive color)
	[SerializeField] private Transform[] m_Cartridges;          // list of the portal cartridges on the device
	[SerializeField] private LayerMask m_IgnoredMask;
	[SerializeField] private bool[] m_EnabledRealities;         // what realities are currently enabled
    [SerializeField] private LevelLoader m_SpawnPos;            // where to respawn
    [SerializeField] private Transform m_BaseObj;               // root object of the player
    [SerializeField] private AudioClip m_PortalOpenSound;       // sound to play when a portal is successfully opened
    [SerializeField] private AudioClip m_PortalFailSound;       // sound to play when portal opening fails, or when a portal is closed
    [SerializeField] private AutoHide[] m_Effects;              // portal opening effects

	private int m_SelectedReality = 0;              // reality that is currently selected
	private int m_CurrentReality = 0;               // reality in which the player is currently
	private float m_RisingAnimationPos = 0.0f;      // handles enabling/disabling portal opening based on the distance from objects

	private Camera m_camera;                                                                                // camera of the fps character
	private GameObject m_OpenPortal;                                                                        // the object of the currently open portal
	private Rigidbody m_PickedUpObject = null;                                                              // the object that has been picked up
	private Color[] m_EmissionColors = {Color.yellow, Color.red, Color.white, Color.cyan, Color.green};     // list of colors for the emissive parts of the texture for each reality
	private Animator m_Animator;
    private float m_AnimationTimer = 0.0f;
    private AudioSource m_AudioSource;

    private FirstPersonController m_FpsController;                          // the fps controller that the player controls
    private List<GameObject> m_RealityParents = new List<GameObject>();     // list of reality world objects
    private bool m_InMenu = false;                                          // is the in-game menu currently open

    private string[] layerNames = {"RealityA", "RealityB", "Reality0g", "RealitySlow", "RealityLiving"};    // layer names for raycasting

    private int sector = 0;                             // current sector
    private int area = 0;                               // current area
    private float m_DeathEffectTimer = 0.0f;            // timer for the death effect
    [SerializeField] private string[] areaNames;        // list of sector names
    [SerializeField] private Image m_DeathEffect;       // the image used to indicate death

	void Start () {
		m_camera = this.GetComponent<Camera>();
		DisplayCartridges();        // display any cartridges if necessary
		UpdateCartridges();         // update the position of cartridges

		if (m_Cartridges.Length != m_EnabledRealities.Length)
			Debug.LogWarning("PortalDevice Cartridges and EnabledRealities lengths are not the same.");
		m_Animator = m_Hand.GetComponent<Animator>();

		if (m_Animator == null)
			Debug.LogError("PortalDevice hand is missing an animator");

        if (m_SpawnPos != null) {
            m_BaseObj.position = m_SpawnPos.transform.position;
            m_BaseObj.rotation = m_SpawnPos.transform.rotation;
        }

        m_AudioSource = this.GetComponent<AudioSource>();
        m_FpsController = this.GetComponentInParent<FirstPersonController>();

        if (m_DeathEffect != null) {
            m_DeathEffect.GetComponent<CanvasRenderer>().SetAlpha(0.0f);    // hide the death effect
        }
	}

	void Update () {
        /**
         * if the player presses the portal opening button, the character is not too close to a wall,
         * the menu is not open and the player is trying to open a portal to a different reality than the current one
         * */
		if (Input.GetButtonDown("Open Portal") && m_RisingAnimationPos == 1.0f && m_CurrentReality != m_SelectedReality && m_AnimationTimer == 0.0f && !m_InMenu){ 

            string[] layersForRay = {"Default", layerNames[m_CurrentReality], layerNames[m_SelectedReality]};
            int rayLayer = LayerMask.GetMask(layersForRay);
            
            /*
             * Check whether or not the portal would intersect any objects
             * */

            bool rightHit = Physics.Raycast(m_camera.transform.position + m_camera.transform.forward * 2.0f, m_camera.transform.right, 1.6f, rayLayer);
            bool leftHit = Physics.Raycast(m_camera.transform.position + m_camera.transform.forward * 2.0f, m_camera.transform.right * -1.0f, 1.6f, rayLayer);
            bool upHit = Physics.Raycast(m_camera.transform.position + m_camera.transform.forward * 2.0f, m_camera.transform.up, 1.6f, rayLayer);
            bool downHit = Physics.Raycast(m_camera.transform.position + m_camera.transform.forward * 2.0f, m_camera.transform.up * -1.0f, 1.6f, rayLayer);

            bool canOpen = true;        // whether or not the portal can be opened (so whether or not it intersects a wall)
            float rightShift = 0.0f;    
            float upShift = 0.0f;
            int loops = 20;     // maximum number of times we try to move the portal around to find an empty place

            while (rightHit || leftHit || upHit || downHit) {
                if ((rightHit && leftHit) || (upHit && downHit)) {  // if the portal is in a corner too close to walls, then it can't be moved enough
                    canOpen = false;
                    break;
                }
                // if we collide on the right, move the portal to left and so on
                if (rightHit)
                    rightShift -= 0.1f;
                if (leftHit)
                    rightShift += 0.1f;
                if (upHit)
                    upShift -= 0.1f;
                if (downHit)
                    upShift += 0.1f;

                rightHit = Physics.Raycast(m_camera.transform.position + m_camera.transform.forward * 2.0f + m_camera.transform.right * rightShift, m_camera.transform.right, 1.6f, rayLayer);
                leftHit = Physics.Raycast(m_camera.transform.position + m_camera.transform.forward * 2.0f + m_camera.transform.right * rightShift, m_camera.transform.right * -1.0f, 1.6f, rayLayer);
                upHit = Physics.Raycast(m_camera.transform.position + m_camera.transform.forward * 2.0f + m_camera.transform.up * upShift, m_camera.transform.up, 1.6f, rayLayer);
                downHit = Physics.Raycast(m_camera.transform.position + m_camera.transform.forward * 2.0f + m_camera.transform.up * upShift, m_camera.transform.up * -1.0f, 1.6f, rayLayer);

                loops -= 1;
                if (loops == 0) {       // if we are out of tries, stop trying to move the portal
                    canOpen = false;
                    break;
                }
            }

            if (canOpen) {      // if the portal wouldn't intersect any walls

                GameObject.Destroy(m_OpenPortal);								//close currently open portal

                GameObject go = null;
                go = GameObject.Instantiate(m_Portals[m_SelectedReality]);		//create new portal from prefab

                go.transform.position = m_camera.transform.position + m_camera.transform.forward * 1.5f + m_camera.transform.right * rightShift + m_camera.transform.up * upShift;	//move portal to correct position
                go.transform.rotation = m_camera.transform.rotation;										//and rotate it correctly
                go.transform.Rotate(new Vector3(-90.0f, 0.0f, 0.0f));

                ChangeReality changeReality = go.GetComponent<ChangeReality>();     // set the portal's old reality
                changeReality.SetOldReality(m_CurrentReality);

                HideRealityObjects();       // enable the rendering of proper realities, and disable others

                m_OpenPortal = go;          // set the newly created portal object as the currently open portal

                m_Animator.SetTrigger("CreatePortal");      // animate the portal device

                m_AudioSource.clip = m_PortalOpenSound;     // play the portal opening sound
                m_AudioSource.Play();
            } else {
                m_Animator.SetTrigger("CreatePortal");      // animate the portal device
                m_AudioSource.clip = m_PortalFailSound;     // play the portal fail sound
                m_AudioSource.Play();
            }
            m_Effects[m_SelectedReality].gameObject.SetActive(true);        // show the portal opening effect
            m_Effects[m_SelectedReality].Hide(0.1f);
        } else {
            if (Input.GetButtonDown("Open Portal") && m_CurrentReality != m_SelectedReality && m_AnimationTimer == 0.0f && !m_InMenu) {     // if the player is facing the wall too close
                m_Animator.SetTrigger("CreatePortal");      // fail to open the portal
                m_AudioSource.clip = m_PortalFailSound;
                m_AudioSource.Play();
            }
        }

        if (m_AnimationTimer > 0.0f) {              // if animation timer is larger than 0 reduce it
            m_AnimationTimer -= Time.deltaTime;
            if (m_AnimationTimer < 0.0f) {
                m_AnimationTimer = 0.0f;
            }
        }

        if (m_DeathEffectTimer > 0.0f) {                // if death effect timer is larger than 0 reduce it
            m_DeathEffectTimer -= Time.deltaTime;
            if (m_DeathEffectTimer <= 0.0f) {           // once it reaches 0, hide the death effect
                m_DeathEffectTimer = 0.0f;
                m_DeathEffect.CrossFadeAlpha(0.0f, 1.0f, true);
            }
        }

		/* choosing realities */
        if (Input.GetButtonDown("Reality A")){
			ChooseReality(0);
		} else if (Input.GetButtonDown("Reality B")){
			ChooseReality(1);
		} else if (Input.GetButtonDown("Reality 0g")){
			ChooseReality(2);
		} else if (Input.GetButtonDown("Reality Slow")){
			ChooseReality(3);
        } else if (Input.GetButtonDown("Reality Living")) {
            ChooseReality(4);
        }

        /* closing portal */
        if (Input.GetButtonDown("Close Portal")) {
            ClosePortal();
            m_AudioSource.clip = m_PortalFailSound;
            m_AudioSource.Play();
        }

        /*
         * Test whether or not the player is too close to an object and facing it
         */
		RaycastHit rayHit;
		bool rc = Physics.Raycast(this.transform.position + this.transform.forward * 0.8f, this.transform.forward, out rayHit, 1.0f, LayerMask.GetMask(new string[] {"Default","RealityA","RealityB","Reality0g","RealitySlow","RealityLiving"})/*m_camera.cullingMask >> LayerMask.NameToLayer("LevelLoadZone")*/);
		if (rc){
			if (!rayHit.collider.tag.Equals("Player") && !rayHit.collider.tag.Equals("PortalTeleport")){

				m_RisingAnimationPos = rayHit.distance - 0.5f;
			}
		} else {
			if (m_RisingAnimationPos < 1.0f){
				m_RisingAnimationPos += 5.0f * Time.unscaledDeltaTime;
			} else {
				m_RisingAnimationPos = 1.0f;
			}
		}

		/* picking up objects */

		if (m_PickedUpObject == null){
			if (Input.GetButton("Pick Up Object")){
                // is there an object in front of us
				RaycastHit pickupHit;
				bool pickupInRange = Physics.Raycast(this.transform.position + this.transform.forward * 0.5f, this.transform.forward, out pickupHit, 2.0f, LayerMask.GetMask(layerNames));

				if (pickupInRange){ // if there is one
					CanPickUp pickupObject = pickupHit.collider.gameObject.GetComponent<CanPickUp>();
					if (pickupObject != null){ // and it is a box
                        if (pickupObject.gameObject.layer == m_BaseObj.gameObject.layer) { // that is in the same reality
                            m_PickedUpObject = pickupObject.gameObject.GetComponent<Rigidbody>();   // then pick it up
                            m_PickedUpObject.angularDrag *= 1000;       // also slow it's rotation
                            Physics.IgnoreCollision(m_PickedUpObject.GetComponent<Collider>(), m_BaseObj.GetComponent<Collider>(), true);   // and disable it's collisions with us
                        } else {    // if the box is in another reality
                            // check whether or not there is a portal between us and the box
                            RaycastHit portalHit;
                            bool portalInRange = Physics.Raycast(this.transform.position, this.transform.forward, out portalHit, pickupHit.distance, LayerMask.GetMask(new string[] { "Portals" }));
                            if (portalInRange) {    // if there is
                                ChangeReality cr = portalHit.collider.gameObject.GetComponent<ChangeReality>();
                                if (cr.GetNewRealityLayer() == pickupObject.gameObject.layer) {     // and the portal leads to the reality that the box is in
                                    Vector3 relativeVector = this.transform.position - cr.transform.position;    // get the vector between portal and object
                                    float angle = Vector3.Angle(relativeVector, cr.transform.up);
                                    if (angle < 90) {       // and we are looking through it (so not behind it or anything)
                                        m_PickedUpObject = pickupObject.gameObject.GetComponent<Rigidbody>();   // pick the box up
                                        m_PickedUpObject.angularDrag *= 1000;
                                        Physics.IgnoreCollision(m_PickedUpObject.GetComponent<Collider>(), m_BaseObj.GetComponent<Collider>(), true);
                                    }
                                }
                            }
                        }
					}
				}
			}
		} else {    // we have a box picked up
			m_PickedUpObject.drag = 0;

            Vector3 targetPos = this.transform.position + this.transform.forward * 2.0f;  // the position where the box should be in front of us
            if (targetPos.y < this.transform.position.y - 1.0f) {  // limit how low the player can lower the box
                targetPos.y = this.transform.position.y - 1.0f;
            }
			m_PickedUpObject.velocity = (targetPos - m_PickedUpObject.transform.position) * 30.0f; // move the box towards it target position

            // check whether or not there is a portal between us and the box
            RaycastHit portalHit;
            bool portalInRange = Physics.Raycast(this.transform.position, m_PickedUpObject.transform.position - this.transform.position, out portalHit, 2.0f, LayerMask.GetMask(new string[] { "Portals" }));

            if (m_PickedUpObject.gameObject.layer != m_BaseObj.gameObject.layer && !portalInRange) { // if there is no portal between us and the box, and the box is in a different reality than us
                if (m_PickedUpObject.velocity.magnitude > 5.0f) {           // limit the box's reality
                    m_PickedUpObject.velocity = m_PickedUpObject.velocity.normalized * 5.0f;
                }
                Physics.IgnoreCollision(m_PickedUpObject.GetComponent<Collider>(), m_BaseObj.GetComponent<Collider>(), false);  // enable collisions between us and the box
                m_PickedUpObject.angularDrag /= 1000;   // restore it's angular drag
                m_PickedUpObject = null;                // and drop it
            }

            if ((targetPos - m_PickedUpObject.transform.position).magnitude > 2.0) {    // drop the box when it gets too far
				if (m_PickedUpObject.velocity.magnitude > 5.0f){
					m_PickedUpObject.velocity = m_PickedUpObject.velocity.normalized * 5.0f;
                }
                Physics.IgnoreCollision(m_PickedUpObject.GetComponent<Collider>(), m_BaseObj.GetComponent<Collider>(), false);
                m_PickedUpObject.angularDrag /= 1000;
				m_PickedUpObject = null;
			}
			if (Input.GetButtonUp("Pick Up Object")){                                   // drop the box when the button is released
				if (m_PickedUpObject.velocity.magnitude > 5.0f){
					m_PickedUpObject.velocity = m_PickedUpObject.velocity.normalized * 5.0f;
                }
                Physics.IgnoreCollision(m_PickedUpObject.GetComponent<Collider>(), m_BaseObj.GetComponent<Collider>(), false);
                m_PickedUpObject.angularDrag /= 1000;
				m_PickedUpObject = null;
			}
		}
	}

	void UpdateCartridges(){    // updates cartridge positions on the device
		int i = 0;
		foreach (Transform cartridge in m_Cartridges){
			if (m_SelectedReality == i){
				cartridge.localPosition = new Vector3(cartridge.localPosition.x, 0.0f, cartridge.localPosition.z);  // move the selected reality cartridge up
			} else {
				cartridge.localPosition = new Vector3(cartridge.localPosition.x, -0.071f, cartridge.localPosition.z);   // and others down
			}
			i++;
		}
		m_DeviceMaterial.SetColor("_EmissionColor", m_EmissionColors[m_SelectedReality]);   // update the emissive color of the device material
	}

	void ChooseReality(int reality){        // chooses a reality
		if (m_EnabledRealities[reality]){   // if the chosen reality is enabled
			m_SelectedReality = reality;    // set it as the selected one
			UpdateCartridges();             // and update the cartridges
		}
	}

	void DisplayCartridges(){                               // shows the cartridges of the realities that are enabled
		for (int i = 0; i < m_Cartridges.Length; i++){
			if (m_EnabledRealities[i]){
				m_Cartridges[i].GetComponent<Renderer>().enabled = true;
			} else {
				m_Cartridges[i].GetComponent<Renderer>().enabled = false;
			}
		}
	}

	public GameObject GetCurrentPortalPrefab(){     // returns the prefab of the currently selected reality
		return m_Portals[m_CurrentReality];
	}

	public void SetReality(int reality){        // sets the current reality to @reality
		m_CurrentReality = reality;
	}

	public void SetOpenPortal(GameObject go){   // sets the current open portal object to @go
		m_OpenPortal = go;
	}

    public void EnableReality(int reality, bool select) {   // enables the reality @reality and selects if if @select==true
        m_EnabledRealities[reality] = true;
        DisplayCartridges();
        if (select)
            ChooseReality(reality);
    }

    public bool isRealityEnabled(int reality) {     // returns whether or not the reality @reality is enabled
        return m_EnabledRealities[reality];
    }

    public void ReceiveDevice(bool select) {        // enables the portal device
        m_Hand.gameObject.SetActive(true);
        EnableReality(1, select);                   // also enable reality B
    }

    /**
     * Respawning
     **/
    public void Respawn(bool deathEffect) {
        if (m_SpawnPos != null) {
            m_BaseObj.position = m_SpawnPos.transform.position;             // move to spawn position
            m_BaseObj.GetComponent<Rigidbody>().velocity = Vector3.zero;    // stop movement
            m_FpsController.StopMovement();
            m_FpsController.SetRotation(m_SpawnPos.transform.rotation.eulerAngles.x, m_SpawnPos.transform.rotation.eulerAngles.y);  // rotate player correctly
            if (deathEffect) {      // if the death effect should be displayed,
                m_DeathEffect.CrossFadeAlpha(1.0f, 0.2f, true); // fade it in
                m_DeathEffectTimer = 1.0f;
            }
        }
    }

    public void SetRespawnPos(LevelLoader ll) { // sets the respawn position to the level loader @ll position
        m_SpawnPos = ll;
        UpdateRealityParents();
    }

    private void UpdateRealityParents() {                   // update reality parents for enabling/disabling the rendering of those realities
        foreach (GameObject r in m_RealityParents) {
            r.SetActive(true);
        }
        m_RealityParents.Clear();
        GameObject[] realities = GameObject.FindGameObjectsWithTag("Reality");  // find all reality objects
        foreach (GameObject r in realities) {   // and add them to the list
            m_RealityParents.Add(r);
        }

        HideRealityObjects();
    }

    private void HideRealityObjects() {         // hides all realities that are not currently necessary (the ones that ARE necessary are: reality that the player is currently in and reality into which a portal is currently open)
        foreach (GameObject r in m_RealityParents) {            
            foreach (Renderer rend in r.GetComponentsInChildren<Renderer>()) {
                if (r.layer == m_CurrentReality + 10 || r.layer == m_SelectedReality + 10) {    // enable the rendering of realities that are necessary
                    rend.enabled = true;
                } else {                        // and disable others
                    rend.enabled = false;
                }
            }
        }
    }

    public void Menu(bool menu) {   // sets whether or not the in-game menu is currently open
        m_InMenu = menu;
    }

    public void ClosePortal() {             // closes currently open portal
        GameObject.Destroy(m_OpenPortal);
    }

    public void UpdateCurrentArea(int s, int a) {       // updates current sector and area
        sector = s;
        area = a;
    }

    public string GetAreaName() {                   // returns current area name
        return areaNames[sector - 1] + "-" + area;
    }
}
