﻿using UnityEngine;
using System.Collections;

public class AutoRotate : MonoBehaviour {

    [SerializeField] float m_Speed; // Rotation speed
    private TimescaleController m_TimescaleController;

    void Start() {
        m_TimescaleController = FindObjectOfType<TimescaleController>();    // Require a timescale controller
        if (m_TimescaleController == null) {
            Debug.LogError("Scene is missing a Timescale Controller");
        }
    }

	void Update () {
        this.transform.Rotate(0.0f, 0.0f, m_Speed * m_TimescaleController.getTimescale());  // Rotate the object around it's z axis
	}
}