﻿using UnityEngine;
using System.Collections;

// Object that can be picked up by the player (currently only boxes)
public class CanPickUp : MonoBehaviour {
    [SerializeField] private GameObject[] m_RealityObjects;     // The ghost objects used to show what reality the object is in
    [SerializeField] private GameObject m_DestructionObject;    // Prefab to instantiate when the object is destroyed
    [SerializeField] private BoxFadeInObject m_RespawnObject;   // Respawn object prefab
    private Vector3 m_StartPos;     // Object starting position
    private Quaternion m_StartRot;  // And rotation

    void Start() {
        EnableRealityObject(0);                 // Enable the ghost object for reality A (because the object is by default in that reality)
        m_StartPos = this.transform.position;
        m_StartRot = this.transform.rotation;
    }

    public void EnableRealityObject(int r) {                    // Enables the ghost object for reality n
        for (int i = 0; i < m_RealityObjects.Length; i++) {
            if (i == r) {
                m_RealityObjects[i].SetActive(true);
            } else {
                m_RealityObjects[i].SetActive(false);
            }
        }
    }

    // When the box get's destroyed (eg by a trap)
    public void DestroyBox() {
        GameObject go = GameObject.Instantiate(m_DestructionObject);            // Instantiate the destruction object
        go.transform.position = this.transform.position;                        // Move it to the same location as this object
        go.transform.rotation = this.transform.rotation;                        // Set the same rotation
        go.layer = this.gameObject.layer;                                       // And layer

        float scale = this.GetComponent<CustomGravity>().GetGravityScale();     // Get the gravityscale of this object
        float timeScale = this.GetComponent<CustomGravity>().GetTimescale();

        CustomGravity[] cgs = go.GetComponentsInChildren<CustomGravity>();      // Get the children objects of the destruction object that have the CustomGravity component
        foreach (CustomGravity cg in cgs) {
            cg.SetGravityScale(scale);                                          // And set their gravity scale
        }

        this.transform.position = m_StartPos;       // Move this object to the starting position
        this.transform.rotation = m_StartRot;       // And set rotation to starting rotation

        Rigidbody rb = this.GetComponent<Rigidbody>();  // Get the rigidbody of this object
        rb.angularVelocity = Vector3.zero;              // and set it's angular velocity
        rb.velocity = Vector3.zero;                     // and velocity to 0

        BoxFadeInObject bfio = (BoxFadeInObject)GameObject.Instantiate(m_RespawnObject);    // Create a respawn object
        bfio.transform.position = this.transform.position;
        bfio.transform.rotation = this.transform.rotation;
        bfio.SetTarget(this.gameObject);                                                    // And set it's target object to be this object

        this.gameObject.SetActive(false);       // Finally disable this object
    }
}
