﻿using UnityEngine;
using System.Collections;

// Serializable object that holds save data
[System.Serializable]
public class SaveData {

	public int unlockedSector = 1;
    public int unlockedLevel = 1;
}
