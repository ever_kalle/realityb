﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

// Shows a help message on screen
[RequireComponent(typeof(Collider))]
public class ShowHelp : MonoBehaviour {

    [SerializeField] private float m_StayTime = 3.0f;           // How many seconds the help message remains on the screen
    [SerializeField] [Multiline] private string m_HelpText;     // Text to display
    [SerializeField] private Image m_Image;                     // Background image object of the message
    [SerializeField] private Text m_Text;                       // Text object of the message

    private bool m_Showed = false;                              // Has the message been displayed already?
    private bool m_Hide = false;                                // Has the message begun it's hiding process

    void Start() {
        if (m_Image != null && m_Text != null) { 
            m_Image.GetComponent<CanvasRenderer>().SetAlpha(0.0f);  // Hide the background image
            m_Text.GetComponent<CanvasRenderer>().SetAlpha(0.0f);   // and text at start
        }
    }

	void Update () {
        if (m_Showed) {
            if (m_StayTime > 0.0f) {
                m_StayTime -= Time.deltaTime;
            } else {
                if (!m_Hide) {  // Once m_StayTime runs out, hide the message if the hiding process has not yet started
                    m_Image.CrossFadeAlpha(0.0f, 1.0f, false);
                    m_Text.CrossFadeAlpha(0.0f, 1.0f, false);
                    m_Hide = true;
                }
            }
        }
	}

    void OnTriggerEnter(Collider other) {       // Show the message on collision
        if (!m_Showed) {
            m_Text.text = m_HelpText;           // Set text
            m_Image.CrossFadeAlpha(0.75f, 1.0f, false); // And fade the message in
            m_Text.CrossFadeAlpha(1.0f, 1.0f, false);
            m_Showed = true;
        }
    }
}
