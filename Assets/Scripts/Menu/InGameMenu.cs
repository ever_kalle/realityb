﻿using UnityEngine;
using System.Collections;
using UnityStandardAssets.Characters.FirstPerson;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

// Controller for the in-game menu
[RequireComponent(typeof(FirstPersonController))]
public class InGameMenu : MonoBehaviour {

    [SerializeField] private GameObject m_Canvas;   // Canvas used by menu
    [SerializeField] private Text m_AreaName;       // Current area name
    private FirstPersonController m_Controller;
    private bool m_Enabled = false;                 // Is the menu currently open
    private PortalDevice m_PD;

	// Use this for initialization
	void Start () {
	    m_Controller = this.GetComponent<FirstPersonController>();  // Get the fps controller
        m_PD = this.GetComponentInChildren<PortalDevice>();         // and portal device 
	}
	
	// Update is called once per frame
	void Update () {
        if (Input.GetKeyUp(KeyCode.Escape)) {       // Toggle the menu with escape
            m_Enabled = !m_Enabled;
            UpdateMenuStatus();
        }
	}

    private void UpdateMenuStatus(){
        m_Canvas.SetActive(m_Enabled);      // Set the canvas to be (in)active
        m_Controller.Menu(m_Enabled);       // Tell the fps controller that the menu has been opened/closed (to disable/enable movement)
        m_PD.Menu(m_Enabled);               // Tell the portaldevice that the menu has been opened/closed (to disable/enable the portal opening)
        m_AreaName.text = m_PD.GetAreaName();   // Get the area name from PortalDevice
    }

    public void Resume() {  // Resume the game (used by the in-game menu button "Resume")
        m_Enabled = false;  // Set menu to be disabled
        UpdateMenuStatus(); // and update the menu status
    }

    public void MainMenu() {    // Going back to the main menu
        SceneManager.LoadScene("MainMenu", LoadSceneMode.Single);
    }

    public void Quit() {        // Quitting the game
        Application.Quit();
    }
}
