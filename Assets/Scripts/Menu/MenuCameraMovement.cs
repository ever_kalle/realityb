﻿using UnityEngine;
using System.Collections;
using UnityStandardAssets.CrossPlatformInput;

// Move the main menu camera slightly when mouse is moved
public class MenuCameraMovement : MonoBehaviour {
    
    [SerializeField] private float m_Sensitivity;           // Multiplier for mouse movement
    [SerializeField] private float m_MaxRot;                // How much can the camera be rotated by mouse movement
    private Vector3 m_RotationEuler;                        // The base rotation of the camera
    private Vector3 m_DeltaRotationEuler = Vector3.zero;    // How much should the camera be rotated this frame

    [SerializeField] private GameObject[] m_Scenes;         // Background scenes of the main menu
    [SerializeField] private LayerMask[] m_SceneLayers;
    [SerializeField] private float[] toShift;

	void Start () {
        m_RotationEuler = this.transform.rotation.eulerAngles;  // Set the base rotation

        int i = Random.Range(0, m_Scenes.Length);
        i = 2;
        m_Scenes[i].SetActive(true);
        m_Scenes[i].transform.position = m_Scenes[i].transform.position + new Vector3(toShift[i], 0f, 0f);
        this.GetComponent<Camera>().cullingMask = m_SceneLayers[i];
	}
	
	void Update () {
        float yRot = CrossPlatformInputManager.GetAxis("Mouse X") * m_Sensitivity * 0.01f;  // Get the mouse x axis value
        float xRot = -CrossPlatformInputManager.GetAxis("Mouse Y") * m_Sensitivity * 0.01f; // Get the mouse y axis value

        yRot *= (m_MaxRot - Mathf.Min(Mathf.Abs(m_DeltaRotationEuler.y), 1.0f));        // the closer the camera rotation is to max rotation the less mouse movement affects the movement
        xRot *= (m_MaxRot - Mathf.Min(Mathf.Abs(m_DeltaRotationEuler.x), 1.0f));

        m_DeltaRotationEuler.x += xRot;
        m_DeltaRotationEuler.y += yRot;

        m_DeltaRotationEuler.x = Mathf.Clamp(m_DeltaRotationEuler.x, -m_MaxRot, m_MaxRot);  // clamp the rotation between max rotation
        m_DeltaRotationEuler.y = Mathf.Clamp(m_DeltaRotationEuler.y, -m_MaxRot, m_MaxRot);

        this.transform.rotation = Quaternion.Euler(m_RotationEuler + m_DeltaRotationEuler);
	}
}
