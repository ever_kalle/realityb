﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;

// Controller for the main menu
public class MenuController : MonoBehaviour {

    private string m_Level = "s1-1";                                                // The level to load when changing scenes
    [SerializeField] private string[] m_Prefixes = {"Introduction", "Unknown"};     // Section names
    [SerializeField] private int[] m_LevelCounts = {4, 15};                         // Area counts in each sections
    [SerializeField] private RectTransform m_AreaSelect;                            // Container for area selection
    [SerializeField] private Button m_Prefab;                                       // Button prefab (used by section/area selection)
    [SerializeField] private GameObject m_Areas;                                    // Area selection scrollbox
    [SerializeField] private GameObject m_Sector;                                   // Section selection scrollbox
    [SerializeField] private RectTransform m_SectorSelect;                          // Container for section selection
    [SerializeField] private GameObject m_LoadingText;                              // Loading text object
    List<GameObject> gos = new List<GameObject>();                                  // List for temporary button storage
    private static SaveData saveData;                                               // Save Data

    void Awake() {
        MenuController[] mcs = FindObjectsOfType<MenuController>();
        foreach (MenuController mc in mcs) {            // Destroy duplicate menu controllers (when returning to main menu)
            if (mc != this) {
                GameObject.Destroy(mc.gameObject);
            }
        }

        DontDestroyOnLoad(transform.gameObject);        // Don't destroy this object on scene change

        if (m_Prefixes.Length != m_LevelCounts.Length) {                                    // Section count and section area number count must match
            Debug.LogError("Prefixes and level counts list lengths must be the same!");
        }

        Load(); // Load save data
    }

    public void SetLoadingScreen(GameObject go) {   // Sets the loading object
        m_LoadingText = go;
    }

    public void NewGame() {             // Starting a new game
        m_Level = "s1-1";               // Start level: section 1 area 1
        m_LoadingText.SetActive(true);  // Show loading text
        SceneManager.LoadScene("World1", LoadSceneMode.Single);
    }

    public void StartLevel(string levelId) {    // Start the level levelId (used by "Choose Level")
        m_Level = levelId;

        if (m_LoadingText != null)
            m_LoadingText.SetActive(true);

        string[] levelParts = levelId.Split('-');
        string worldNum = levelParts[0].Replace("s", "");                   // Get world number from levelId string
        SceneManager.LoadScene("World" + worldNum, LoadSceneMode.Single);   // Load the proper scene for the world
    }

    public void Continue() {                                                // Continue game
        m_Level = "s" + saveData.unlockedSector + "-" + saveData.unlockedLevel;     // Create levelId string from save data.
        m_LoadingText.SetActive(true);
        SceneManager.LoadScene("World" + saveData.unlockedSector, LoadSceneMode.Single);    // Load proper scene
    }

    public string GetLevel() {  // get the current level
        return m_Level;
    }

    public void MenuSelectLevel() {     // Clicking "Choose Level" button
        m_Sector.SetActive(true);       // Enable section selection scrollbox

        Button b;
        for (int i = 0; i < m_Prefixes.Length; i++) {           // Create buttons for each section
            b = (Button)GameObject.Instantiate(m_Prefab);       // 

            int captured = i;                                   // Need to "capture" the i value because otherwise adding listeners is weird

            b.transform.SetParent(m_SectorSelect);              // Add button to section selection parent
            m_Areas.SetActive(true);

            RectTransform rt = b.GetComponent<RectTransform>(); 
            rt.localPosition = new Vector3(240.0f, -50.0f - 70.0f * i, 0.0f);   // Set the button to correct place
            rt.localScale = new Vector3(1.0f, 1.0f, 1.0f);

            Text t = b.GetComponentInChildren<Text>();

            if (saveData.unlockedSector > i) {      // If the player has reached the sector set the button text to sector name and onclick action to 
                t.text = m_Prefixes[i];                                             // proper area selection
                b.onClick.AddListener(() => MenuSelectSection(captured));
            } else {
                t.text = "LOCKED";                              // otherwise the area is locked
                b.GetComponent<Image>().color = Color.gray;
            }
        }
        m_SectorSelect.sizeDelta = new Vector2(0.0f, 70.0f * (m_Prefixes.Length + 1) - 50.0f);  // Set the scroll area height
    }

    public void MenuSelectSection(int section) {    // Clicking a sector name after "Choose Level"
        Button b;
        foreach (GameObject go in gos){     // Destroy old area buttons if they exist
            GameObject.Destroy(go);
        }
        gos.Clear();

        for (int i = 0; i < m_LevelCounts[section]; i++) {  // Create buttons for each area of the section
            b = (Button)GameObject.Instantiate(m_Prefab);
            gos.Add(b.gameObject);                          // Add the button to the buttons list

            string captured = "" + (i + 1);                 // Again, need to capture the i value for listener
            b.transform.SetParent(m_AreaSelect);

            m_Areas.SetActive(true);
            RectTransform rt = b.GetComponent<RectTransform>();
            rt.localPosition = new Vector3(240.0f, -50.0f - 70.0f * i, 0.0f);   // Move the button to correct location
            rt.localScale = new Vector3(1.0f, 1.0f, 1.0f);

            Text t = b.GetComponentInChildren<Text>();

            if (saveData.unlockedLevel > i || saveData.unlockedSector > section + 1) {  // If the area is unlocked
                t.text = "" + (i+1);                                                                // Set proper text
                b.onClick.AddListener(() => StartLevel("s" + (section + 1) + "-" + captured));      // And onclick listener to load the area
            } else {
                t.text = "LOCKED";                              // Otherwise the area is locked
                b.GetComponent<Image>() .color = Color.gray;
            }
        }

        m_AreaSelect.sizeDelta = new Vector2(0.0f, 70.0f * (m_LevelCounts[section] + 1) - 50.0f);  // Set the scroll area height
    }
    
    // Save the game
    public static void Save() {
        BinaryFormatter bf = new BinaryFormatter();
        FileStream file = File.Create(Application.persistentDataPath + "/saveData.gd");
        bf.Serialize(file, saveData);
        file.Close();
    }

    // Load the game
    public static void Load() {
        if (File.Exists(Application.persistentDataPath + "/saveData.gd")) {
            BinaryFormatter bf = new BinaryFormatter();
            FileStream file = File.Open(Application.persistentDataPath + "/saveData.gd", FileMode.Open);
            saveData = (SaveData)bf.Deserialize(file);
            file.Close();
        } else {
            saveData = new SaveData();
        }
    }

    // Unlock a level
    public static void UnlockLevel(int sector, int area) {
        if (saveData.unlockedSector < sector) { // if a new section is reached
            saveData.unlockedSector = sector;
            saveData.unlockedLevel = area;
        } else {    // Still within the same section
            if (sector == saveData.unlockedSector) {
                if (area >= saveData.unlockedLevel) {   // If the new area is later than the previous unlocked area of the section
                    saveData.unlockedSector = sector;
                    saveData.unlockedLevel = area;
                }
            }
        }
        Save(); //  Save the game
    }

    // Pressing "Quit"
    public void Quit() {
        Application.Quit();
    }

}
