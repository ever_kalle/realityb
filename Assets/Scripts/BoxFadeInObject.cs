﻿using UnityEngine;
using System.Collections;

// The box respawn animation object
public class BoxFadeInObject : MonoBehaviour {

    private GameObject m_Target;    // The box to move back to starting position after respawn animation ends
	[SerializeField] private Material m_RespawnMaterial;    // Material used by the respawn object
    private float m_Treshhold = 0.0f;

    void Start() {
        m_Treshhold = 0.0f;
        m_RespawnMaterial = this.GetComponent<Renderer>().sharedMaterial;   // Get the material from this object's renderer
        m_RespawnMaterial.SetFloat("_Threshold", m_Treshhold);              // Set the cutoff threshold of the material
    }

    public void SetTarget(GameObject t) {
        m_Target = t;
    }
	
	// Update is called once per frame
	void Update () {
        if (m_Treshhold < 1.0f) {
            m_Treshhold += Time.deltaTime;                                  // Increase the cutoff threshold
            m_RespawnMaterial.SetFloat("_Threshold", m_Treshhold);          // And set it to the material
        } else {
            m_Target.SetActive(true);                       // If respawn animation is complete, enable the actual box
            GameObject.Destroy(this.gameObject);            // And destroy this respawn object
        }
	}
}
