﻿using UnityEngine;
using System.Collections;

// An object that can be triggered by a floor button
public class Triggerable : MonoBehaviour {
	public virtual void TriggerStart(){}    // Method for when the button trigger starts
	public virtual void TriggerEnd(){}      // and when the trigger ends
}
