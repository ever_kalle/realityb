﻿using UnityEngine;
using System.Collections;

// Pickup object for the portal device
[RequireComponent(typeof(Collider))]
public class DevicePickupObject : MonoBehaviour {

    void OnTriggerEnter(Collider other) {
        string otherTag = other.gameObject.tag;
        if (otherTag.Equals("Player")) {        // if we enter a collision with the player
            PortalDevice pd = other.GetComponentInChildren<PortalDevice>();
            pd.ReceiveDevice(true); // enable the portal device
            GameObject.Destroy(this.gameObject);    // destroy the pickup object
        }
    }
}
