﻿using UnityEngine;
using System.Collections;

// Timescale controller
public class TimescaleController : MonoBehaviour {

	[SerializeField] private float m_SlowTimeScale = 0.1f;  // The timescale for slow time
	private float m_CurrentTimescale = 1.0f;                // Current timescale

	public float getTimescale(){
		return m_CurrentTimescale;
	}

	public float getSlowTimeConst(){
		return m_SlowTimeScale;
	}

	public void slowDown(){                     // Slows the time down (set current timescale to the slow time scale)
		m_CurrentTimescale = m_SlowTimeScale;
	}

	public void speedUp(){                      // Sets current time scale back to 1.0f (normal speed)
		m_CurrentTimescale = 1.0f;
	}
}
