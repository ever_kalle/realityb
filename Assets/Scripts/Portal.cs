﻿using UnityEngine;
using System.Collections;

// Handles portal rendering
[ExecuteInEditMode]
[RequireComponent (typeof(Renderer))]
public class Portal : MonoBehaviour {
	[SerializeField] private LayerMask m_CullingMask;
	public float clipPlaneOffset = 0.00F;

	private static bool m_IsRendering = false;
	private RenderTexture m_RenderTexture;
	private Camera m_PortalCamera;

	void Start() {
		m_RenderTexture = new RenderTexture(2048,2048,24);  // create a render texture
		m_RenderTexture.hideFlags = HideFlags.DontSave;

		GameObject cameraGameObject = new GameObject();     // create the portal camera
		cameraGameObject.AddComponent<Camera>();
		cameraGameObject.transform.parent = this.transform;
		m_PortalCamera = cameraGameObject.GetComponent<Camera>();

		m_PortalCamera.enabled = false;
		m_PortalCamera.targetTexture = m_RenderTexture;
		m_PortalCamera.cullingMask = m_CullingMask;

		this.gameObject.GetComponent<Renderer>().sharedMaterial.SetTexture("_MainTex", m_PortalCamera.targetTexture);   // set the portal render texture to the renderer material
	}

	public void OnWillRenderObject() {      // if the portal is being rendered right now
		Camera camera = Camera.current;
		if (!camera || m_IsRendering){
			return;
		}
		CopyCameraSettings( camera );       // set the portal camera settings to be the same as the camera's that is rendering it
		m_IsRendering = true;
		m_PortalCamera.worldToCameraMatrix = camera.worldToCameraMatrix;

		Vector3 pos = this.transform.position;
		Vector3 normal = this.transform.up * -1.0f;

		m_PortalCamera.projectionMatrix = ObliqueFrustumClippingMatrix(m_PortalCamera, pos, normal);    // apply OFC to camera's projection matrix

		m_PortalCamera.Render();
		m_IsRendering = false;
	}

	void OnDestroy(){
		if (m_PortalCamera != null){                        // destroy portal camera when the portal is destroyed
			GameObject.Destroy(m_PortalCamera.gameObject);
		}
		m_RenderTexture.Release();          // and release the render texture
	}

	private void CopyCameraSettings(Camera source)          // sets the portal camera settings to be the same as the source camera
	{		
		m_PortalCamera.clearFlags = source.clearFlags;
		m_PortalCamera.backgroundColor = source.backgroundColor;

		m_PortalCamera.farClipPlane = source.farClipPlane;
		m_PortalCamera.nearClipPlane = source.nearClipPlane;

		m_PortalCamera.fieldOfView = source.fieldOfView;
		m_PortalCamera.aspect = source.aspect;

		m_PortalCamera.transform.position = source.transform.position;
	}

	private Matrix4x4 ObliqueFrustumClippingMatrix(Camera camera, Vector3 clipPlanePosition, Vector3 clipPlaneNormal){      // OFC
		Matrix4x4 projectionMatrix = camera.projectionMatrix;	//Get camera's current projection matrix

		//Calculate clipping plane position and normal in camera space
		Vector3 clipPlanePositionC = camera.worldToCameraMatrix.MultiplyPoint(clipPlanePosition);
        Vector3 clipPlaneNormalC = camera.worldToCameraMatrix.MultiplyVector(clipPlaneNormal).normalized;

		Vector4 clipPlane = new Vector4(clipPlaneNormalC.x, clipPlaneNormalC.y, clipPlaneNormalC.z, -Vector3.Dot(clipPlanePositionC, clipPlaneNormalC));

		Vector4 q = projectionMatrix.inverse * new Vector4(Sign(clipPlane.x), Sign(clipPlane.y), 1.0f, 1.0f);

		//Calculate the new third row of the projection matrix
		Vector4 thirdRow = ((-2.0f * q.z) / Vector4.Dot(clipPlane, q)) * clipPlane + new Vector4(0.0f, 0.0f, 1.0f, 0.0f);
		projectionMatrix.SetRow(2, thirdRow);   // set the projection matrix third row to be the new third row
		return projectionMatrix;
	}

	private float Sign(float f){        // Custom sign function (the unity Mathf.Sign() does not return 0.0f when the argument is 0.0f)
		if (f == 0.0f)
			return 0.0f;
		return Mathf.Sign(f);
	}
}
