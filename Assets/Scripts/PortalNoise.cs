﻿using UnityEngine;
using System.Collections;

// Creates a texture using perlin noise to be used by the portal animation
public class PortalNoise : MonoBehaviour {
	public int m_TextureSize;

	//How much to shift the sampling coordiantes
	private float[] xShift = {0.0f, 0.0f, 0.0f};
	private float[] yShift = {0.0f, 0.0f, 0.0f};

	//How much the sampling coordinate shift is changed per second
	private float[] xShiftSpeed = {0.1f, -2.0f, 1.0f};
	private float[] yShiftSpeed = {0.2f, 1.0f, -0.5f};

	//Scales for different noise layers
	private float[] m_Scales = {5.0f, 25.0f, 50.0f};

	private Texture2D m_NoiseTex;
	private Color[] m_ColorValues;

	void Start() {
		m_NoiseTex = new Texture2D(m_TextureSize, m_TextureSize);
		this.gameObject.GetComponent<Renderer>().sharedMaterial.SetTexture("_NoiseTex", m_NoiseTex);    // Set the texture of the material

		m_ColorValues = new Color[m_NoiseTex.width * m_NoiseTex.height];
	}


	/**
	 * Returns Perlin Noise value of the specified noise layer at coordinates (x, y);
	 **/
	float GetNoiseValue(int layer, float x, float y){
		float xCoord = xShift[layer] + x / m_NoiseTex.width * m_Scales[layer];
		float yCoord = yShift[layer] + y / m_NoiseTex.height * m_Scales[layer];
		return Mathf.PerlinNoise(xCoord, yCoord);
	}

    /**
     * Shifts noise layers to animate
     **/
	void UpdateNoiseShift(){
		for(int i = 0; i < xShift.Length; i++){
			xShift[i] += xShiftSpeed[i] * Time.deltaTime;
			yShift[i] += yShiftSpeed[i] * Time.deltaTime;
		}
	}

    /**
     * Calculate noise only if the portal is visible
     **/
	public void OnWillRenderObject() {
		for(float y = 0.0f; y < m_NoiseTex.height; y++) {
			for(float x = 0.0f; x < m_NoiseTex.width; x++) {
				float val1 = GetNoiseValue(0, x, y);
				float val2 = GetNoiseValue(1, x, y);
				float val3 = GetNoiseValue(2, x, y);
				m_ColorValues[(int)y * m_NoiseTex.width + (int)x] = new Color(1.0f, 1.0f, 1.0f, val1 + val2 * 0.5f - val3 * 0.1f);
			}
		}
		m_NoiseTex.SetPixels(m_ColorValues);    // Set texture pixel values to the generated noise values
		m_NoiseTex.Apply();
		UpdateNoiseShift(); // Shift noise layers
	}
}