﻿using UnityEngine;
using System.Collections;

public class AutoHideRenderer : MonoBehaviour {

    [SerializeField] private float m_Timer; // Automatically disables the renderer of this gameobject after m_Timer seconds
    private bool m_HasHidden = false;

    void Update() {
        if (m_Timer > 0.0f) {
            m_Timer -= Time.deltaTime;
        } else {
            if (!m_HasHidden) { 
                this.GetComponent<Renderer>().enabled = false;
                m_HasHidden = true;
            }
        }
    }
}
