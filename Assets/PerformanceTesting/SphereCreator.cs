﻿using UnityEngine;
using System.Collections;


public class SphereCreator : MonoBehaviour {
    [SerializeField]
    private float m_Distance = 0.1f;

    [SerializeField]
    private int m_Xcount = 50;

    [SerializeField]
    private int m_Ycount = 50;

    [SerializeField]
    private int m_Zcount = 50;

    [SerializeField]
    private GameObject m_Template;

    [SerializeField]
    private GameObject m_Template2;

    [SerializeField]
    private bool m_Create1 = true;

    [SerializeField]
    private bool m_Create2 = true;

    private ArrayList m_Times = new ArrayList();
    private int m_CurrentFrame;
    private bool m_Displayed = false;
    private int m_WaitFrame = 30;

	// Use this for initialization
	void Start () {
        for (int i = 0; i < m_Xcount; i++) {
            for (int j = 0; j < m_Ycount; j++){
                for (int k = 0; k < m_Zcount; k++){
                    if (m_Create1) { 
                        GameObject go = GameObject.Instantiate(m_Template);
                        go.transform.position = this.transform.position + new Vector3(m_Distance * -i, m_Distance * j, m_Distance * -k);
                    }
                    if (m_Create2) {
                        GameObject go2 = GameObject.Instantiate(m_Template2);
                        go2.transform.position = this.transform.position + new Vector3(m_Distance * -i - m_Distance * 0.5f, m_Distance * j, m_Distance * -k);
                    }
                }
            }
        }
	}
	
	// Update is called once per frame
	void Update () {
        if (m_WaitFrame > 0)
        {
            m_WaitFrame--;
        }
        else { 
        if (m_CurrentFrame <= 500){
            if (m_CurrentFrame > 0)
                m_Times.Add(Time.unscaledDeltaTime * 1000.0f);
            m_CurrentFrame++;
        }
        else
        {
                if (!m_Displayed) { 
                    float min = 9999999.0f;
                    float max = 0.0f;
                    float tot = 0.0f;

                    foreach (object m in m_Times)
                    {
                        float x = (float)m;
                        if (x < min)
                            min = x;
                        if (x > max)
                            max = x;
                        tot += x;
                    }
                    float avg = tot / 500.0f;
                    Debug.Log("min: " + min + "ms, max: " + max + "ms, average: " + avg);
                    m_Displayed = true;
                }
            }
        }
            
	}
}
